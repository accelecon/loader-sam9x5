//-------------------------------------------------------------------------------------------
//  flashatmel.h
//-------------------------------------------------------------------------------------------

FLASH_Status AF_Init ( bit32 PageSize);
void         AF_Print( void);

FLASH_Status AF_Erase ( bit32 FlashAddress, bit32 FlashBytes);
FLASH_Status AF_Clear ( bit32 FlashAddress, bit32 FlashBytes);
FLASH_Status AF_Write ( bit32 FlashAddress, bit32 FlashBytes);
FLASH_Status AF_Verify( bit32 FlashAddress, bit32 FlashBytes);
FLASH_Status AF_Read  ( bit32 FlashAddress, bit32 FlashBytes, bptr ReadBuffer);

extern const cptr AtmelFlash;

// AtmelFlash - AT45DB011 - 1 Mb, 128 KB.  Uniform 264-byte blocks
// which can be individually erased and programmed.

//#define BOOT_BLOCKSIZE     512 // bootloader physical block size

#define AF_BLOCKSIZE       264 // physical block size in flash
#define AF_BLOCKS_TOTAL    512 // blocks in AT45DB011 flash chip

#define AF_BLOCKS_LOADER    70 //  18,440 bytes
#define AF_BLOCKS_UBOOT    434 // 114,576 bytes
#define AF_BLOCKS_UBOOTENV   8 //   2,112 bytes

#define AF_BLOCK_LOADER	   0
#define AF_BLOCK_UBOOT    (AF_BLOCK_LOADER + AF_BLOCKS_LOADER)
#define AF_BLOCK_UBOOTENV (AF_BLOCK_UBOOT  + AF_BLOCKS_UBOOT )

#define AF_ADDR_LOADER    (AF_BLOCK_LOADER   * AF_BLOCKSIZE)
#define AF_ADDR_UBOOT     (AF_BLOCK_UBOOT    * AF_BLOCKSIZE)
#define AF_ADDR_UBOOTENV  (AF_BLOCK_UBOOTENV * AF_BLOCKSIZE)

#define AF_BYTES_LOADER   (AF_BLOCKS_LOADER  * AF_BLOCKSIZE)
#define AF_BYTES_UBOOT    (AF_BLOCKS_UBOOT   * AF_BLOCKSIZE)

//-------------------------------------------------------------------------------------------
// end
//-------------------------------------------------------------------------------------------

