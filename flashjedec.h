//-------------------------------------------------------------------------------------------
//  flashjedec.h
//-------------------------------------------------------------------------------------------

FLASH_Status JF_Init ( bit32 PageSize);
void         JF_Print( void);

FLASH_Status JF_Erase ( bit32 FlashAddress, bit32 FlashBytes, bit8 Trace);
FLASH_Status JF_Clear ( bit32 FlashAddress, bit32 FlashBytes, bit8 Trace);
FLASH_Status JF_Write ( bit32 FlashAddress, bit32 FlashBytes, bit8 Trace);
FLASH_Status JF_Verify( bit32 FlashAddress, bit32 FlashBytes, bit8 Trace);
FLASH_Status JF_Read  ( bit32 FlashAddress, bit32 FlashBytes, bptr ReadBuffer, bit32 TraceKBytes);

extern const cptr JedecFlash;

// Numonyx SPI Flash - M25P128 - 128 Mb, 16 MB.  Uniform 256-byte
// pages for programming, uniform erase blocks of 256 KB.
// Erase details are handled by low-level implementation, but
// things won't work unless we honor these restrictions in how
// we lay out our regions in the flash.

#define JF_BLOCKSIZE    KB(256) // block size in flash
#define JF_BLOCKS_TOTAL     64  // blocks in M25P128 flash chip

#define JF_BLOCKS_IMAGE0    31  // 7936 KB, on 256 KB boundary 
#define JF_BLOCKS_IMAGE1    31  // 7936 KB, on 256 KB boundary
#define JF_BLOCKS_CONFIG     2  //  512 KB, on 256 KB boundary

#define JF_BLOCK_IMAGE0	   0
#define JF_BLOCK_IMAGE1	  (JF_BLOCK_IMAGE0 + JF_BLOCKS_IMAGE0)
#define JF_BLOCK_CONFIG	  (JF_BLOCK_IMAGE1 + JF_BLOCKS_IMAGE1)

#define JF_ADDR_IMAGE0    (JF_BLOCK_IMAGE0   * JF_BLOCKSIZE)
#define JF_ADDR_IMAGE1    (JF_BLOCK_IMAGE1   * JF_BLOCKSIZE)
#define JF_ADDR_CONFIG    (JF_BLOCK_CONFIG   * JF_BLOCKSIZE)

// Arbitrary values, but avoid collisions with DataFlash device identifiers $00-$3f (see
// flashatmel.h).

#define S25FL064P 0x47  // arbitrary - 2**N for  64 Mbit part + 0x40 for Spansion
#define M25P128P  0x58  // arbitrary - 2**N for 128 Mbit part + 0x50 for Numonyx

//-------------------------------------------------------------------------------------------
// end
//-------------------------------------------------------------------------------------------

