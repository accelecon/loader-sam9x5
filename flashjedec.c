//-------------------------------------------------------------------------------------------
//  flashjedec.c - Spansion/Numonyx S25FL/M25FL-series JEDEC SPI flash support
//-------------------------------------------------------------------------------------------

#include "main.h"

const cptr JedecFlash = "Numonyx";

// Flash interface data.

static struct {
  SpiData Spi       ;
  bit8    TypeCode  ;
  bit8    Status    ;
  bit32   PageSize  ;
  bit32   BlockSize ;
  bit32   SectorSize;
  bit32   TotalBytes;
} JF;

//-------------------------------------------------------------------------------------------
// Spansion/Numonyx SPI Flash interface layer.  Atmel DataFlash emulation layer follows.
//-------------------------------------------------------------------------------------------

#define JEDEC_00_SPANSION 0x01
#define JEDEC_00_NUMONYX  0x20

#define S25FL064P_01 0x02
#define S25FL064P_02 0x16

#define M25P128P_01 0x20
#define M25P128P_02 0x18

#define OPCODE_WRSR       0x01  // write status register
#define OPCODE_PP         0x02  // 256-byte page program
#define OPCODE_READ       0x03  // read
#define OPCODE_RDSR       0x05  // read status register
#define OPCODE_WREN       0x06  // write enable
#define OPCODE_READ_FAST  0x0b  // read fast
#define OPCODE_P4E        0x20  // 4 KB parameter block erase
#define OPCODE_RDID       0x9f  // JEDEC read id
#define OPCODE_SE         0xd8  // 64 KB sector erase

#define RDSR_WIP    0x01  // write in progress
#define RDSR_WEL    0x02  // write enable latch
#define RDSR_BP0    0x04  // block protect 0
#define RDSR_BP1    0x08  // block protect 1
#define RDSR_BP2    0x10  // block protect 2
#define RDSR_E_ERR  0x20  // erase error occurred
#define RDSR_P_ERR  0x40  // programming error occurred
#define RDSR_SRWD   0x80  // status register write disable

// Wait for any write or erase operation to complete.

static FLASH_Status JF_WaitReady( void) {
//bit32 Timeout = MasterClock / 1200;
  bit32 Timeout = MasterClock;
  JF.Status = 0;
  JF.Spi.DataSize = 0;
  do {
    JF.Spi.Command[0] = OPCODE_RDSR;
    JF.Spi.Command[1] = 0;
    if (SPI_SpiReadWrite( 2, &JF.Spi) == SPI_OK) {
      JF.Status = JF.Spi.Command[1];
    }
    Timeout--;
  } while ((JF.Status & RDSR_WIP) && Timeout);
  return (JF.Status & RDSR_WIP) ? FLASH_ERROR : FLASH_OK;
}

// Send an SPI command which carries an address.

static FLASH_Status JF_SpiReadWriteBuffer( bit8 OpCode, bit32 FlashAddress, bptr FlashBuffer, bit32 FlashBytes) {
  JF.Spi.Command[0] = OpCode;
  JF.Spi.Command[1] = (bit8) ((FlashAddress & 0x00ff0000) >> 16);
  JF.Spi.Command[2] = (bit8) ((FlashAddress & 0x0000ff00) >>  8);
  JF.Spi.Command[3] = (bit8) ( FlashAddress & 0x000000ff)       ;
  JF.Spi.Command[4] = 0;
  JF.Spi.Data     = FlashBuffer;
  JF.Spi.DataSize = FlashBytes ;
  return (SPI_SpiReadWrite( OpCode == OPCODE_READ_FAST ? 5 : 4, &JF.Spi) == SPI_OK) ? FLASH_OK : FLASH_ERROR;
}

// Set the write-enable bit in the control register.  This must be done
// before all erase and write commands.

static FLASH_Status JF_WriteEnable( void) {
  JF.Spi.Command[0] = OPCODE_WREN;
  return (SPI_SpiReadWrite( 1, &JF.Spi) == SPI_OK) ? FLASH_OK : FLASH_ERROR;
}

// Erase a block (a section of a sector) and wait for the operation to complete.  This is
// valid for Spansion devices, and even then only in the first 128 KB of flash (called the
// parameter region).  For Numonyx devices this code will never execute because the block
// size is the same as the sector size.

static FLASH_Status JF_BlockErase( bit32 FlashAddress) {
  if (FlashAddress % JF.BlockSize ) {
    TtyPutStr("Address not on block boundary!");
    return FLASH_ERROR;
  }
  if (FlashAddress > 0x0001ffff   ) {
    TtyPutStr("Flash Address out of range!");
    return FLASH_ERROR;
  }
  if (JF_WriteEnable() != FLASH_OK) {
    TtyPutStr("Could not enable write!");
    return FLASH_ERROR;
  }
  if (JF_SpiReadWriteBuffer( OPCODE_P4E, FlashAddress, 0, 0) != FLASH_OK) {
    TtyPutStr("Sending Block Erase command failed!");
    return FLASH_ERROR;
  }
  return JF_WaitReady();
}

// Erase a 64 KB sector and wait for the operation to complete.

static FLASH_Status JF_SectorErase( bit32 FlashAddress) { 
  if (FlashAddress % JF.SectorSize) {
    TtyPutStr("Address not on sector boundary!");
    return FLASH_ERROR;
  }
  if (JF_WriteEnable() != FLASH_OK) {
    TtyPutStr("Could not enable write!");
    return FLASH_ERROR;
  }
  if (JF_SpiReadWriteBuffer( OPCODE_SE, FlashAddress, 0, 0) != FLASH_OK) {
    TtyPutStr("Sending Sector Erase command failed!");
    return FLASH_ERROR;
  }
  return JF_WaitReady();
}

// Write a 256-byte page and wait for the operation to complete.

static FLASH_Status JF_PageWrite( bit32 FlashAddress, bptr FlashBuffer) {
  if (FlashAddress % JF.PageSize  ) return FLASH_ERROR;
  if (JF_WriteEnable() != FLASH_OK) return FLASH_ERROR;
  if (JF_SpiReadWriteBuffer( OPCODE_PP, FlashAddress, FlashBuffer, JF.PageSize) != FLASH_OK) return FLASH_ERROR;
  return JF_WaitReady();
}

// Write a block of 256-byte pages.

static FLASH_Status JF_BlockWrite( bit32 FlashAddress, bptr FlashBuffer) {
  if (FlashAddress % JF.BlockSize) return FLASH_ERROR;
  bit32 BlockOffset;
  for (BlockOffset = 0; BlockOffset < JF.BlockSize; BlockOffset += JF.PageSize) {
    if (JF_PageWrite( FlashAddress, FlashBuffer) != FLASH_OK) return FLASH_ERROR;
    FlashBuffer  += JF.PageSize;
    FlashAddress += JF.PageSize;
  }
  return FLASH_OK;
}

// Return a non-zero code indicating the type of flash device found or
// zero if no supported device is found.

static bit8 JF_Probe( void) {
  JF.Spi.Command[0] = OPCODE_RDID;
  JF.Spi.Command[1] = 0;
  JF.Spi.Command[2] = 0;
  JF.Spi.Command[3] = 0;
  if (SPI_SpiReadWrite( 4, &JF.Spi) == SPI_OK) {
    switch (JF.Spi.Command[1]) {
      case JEDEC_00_SPANSION:
        if ((JF.Spi.Command[2] == S25FL064P_01) && (JF.Spi.Command[3] == S25FL064P_02)) return S25FL064P;
        break;
      case JEDEC_00_NUMONYX:
        if ((JF.Spi.Command[2] == M25P128P_01) && (JF.Spi.Command[3] == M25P128P_02)) return M25P128P;
        break;
  } }
  return 0;
}

// Identify the flash device and prepare it for use.

FLASH_Status JF_Init( bit32 PageSize) {
  SPI_SpiInit( BOARD_FLASH_CS_JEDEC, &JF.Spi);
  switch (JF.TypeCode = JF_Probe()) {
    case S25FL064P: JF.TotalBytes = MB( 8); JF.SectorSize = KB( 64); JF.PageSize = 256; JF.BlockSize = KB(  4); break; //  64 Mb,  8 MB
    case M25P128P : JF.TotalBytes = MB(16); JF.SectorSize = KB(256); JF.PageSize = 256; JF.BlockSize = KB(256); break; // 128 Mb, 16 MB
    default: return FLASH_ERROR;
  }
  return JF.BlockSize == PageSize ? FLASH_OK : FLASH_ERROR;
}

// Print the flash device identifier.

void JF_Print( void) {
  switch (JF.TypeCode) {
    case S25FL064P: TtyPutStr( "  Spansion S25FL064P"); break;
    case M25P128P : TtyPutStr( "   Numonyx M25P128P "); break;
} }

// Erase FlashBytes (a multiple of JF.BlockSize) in flash at address FlashAddress (also a
// multiple of JF.BlockSize).

FLASH_Status JF_Erase( bit32 FlashAddress, bit32 FlashBytes, bit8 Trace) {
  if ((FlashAddress + FlashBytes) > JF.TotalBytes) return FLASH_MEMORY_OVERFLOW;
  if ( FlashAddress               % JF.BlockSize ) return FLASH_ERROR;
  if ( FlashBytes                 % JF.BlockSize ) return FLASH_ERROR;
  SPI_SpiInit( BOARD_FLASH_CS_JEDEC, &JF.Spi);
  while (FlashBytes) {
    if ((JF.TypeCode == M25P128P) || (FlashAddress && ((FlashAddress % JF.SectorSize) == 0))) {
      if (Trace) {
        TtyPutDec( JF.SectorSize / 1024, 1);
        TtyPutStr( "...");
      }
      if (JF_SectorErase( FlashAddress) != FLASH_OK) {
        TtyPutStr("Could not erase sector.");
        return FLASH_ERROR;
      }
      if (FlashBytes > JF.SectorSize) {
        FlashBytes   -= JF.SectorSize;
        FlashAddress += JF.SectorSize;
      } else {
        FlashBytes = 0;
      }
    } else {
      if (Trace) {
        TtyPutDec( JF.BlockSize / 1024, 1);
        TtyPutStr( "...");
      }
      if (JF_BlockErase( FlashAddress) != FLASH_OK){
        TtyPutStr("Could not erase block.");
        return FLASH_ERROR;
      }
      FlashBytes   -= JF.BlockSize;
      FlashAddress += JF.BlockSize;
  } }
  return FLASH_OK;
}

// Read FlashBytes from flash at address FlashAddress.

FLASH_Status JF_Read( bit32 FlashAddress, bit32 FlashBytes, bptr ReadBuffer, bit32 TraceKBytes) {
  if ((FlashAddress + FlashBytes) > JF.TotalBytes) return FLASH_MEMORY_OVERFLOW;
  SPI_SpiInit( BOARD_FLASH_CS_JEDEC, &JF.Spi);
  if (TraceKBytes == 0) {
    return JF_SpiReadWriteBuffer( OPCODE_READ_FAST, FlashAddress, ReadBuffer, FlashBytes);
  }
  bit32 TraceBytes = KB(TraceKBytes);
  while (FlashBytes) {
    bit32 ReadBytes = (FlashBytes > TraceBytes) ? TraceBytes : FlashBytes;
    if (JF_SpiReadWriteBuffer( OPCODE_READ_FAST, FlashAddress, ReadBuffer, ReadBytes) != FLASH_OK) return FLASH_ERROR;
    TtyPutDec( TraceKBytes, 1);
    TtyPutStr( "...");
    FlashAddress += ReadBytes;
    ReadBuffer   += ReadBytes;
    FlashBytes   -= ReadBytes;
  }
  return FLASH_OK;
}

// Verify that FlashBytes of flash at address FlashAddress (a multiple of
// JF.BlockSize) are clear (erased to 0xff).

FLASH_Status JF_Clear( bit32 FlashAddress, bit32 FlashBytes, bit8 Trace) {
  bptr FlashBuffer = ADDRESS(BOARD_SDRAM_BASE) + MB(16);
  bit32 Offset;
  if ( FlashAddress               % JF.BlockSize ) return FLASH_ERROR;
  if (JF_Read( FlashAddress, FlashBytes, FlashBuffer, 256) != FLASH_OK) return FLASH_ERROR;
  if (AllFoxes( FlashBuffer, FlashBytes, &Offset) == 0) {
    TtyPutStr( "AllFoxes failed at $");
    TtyPutHex( FlashAddress+Offset, 6);
    TtyPutLn();
    Offset &= ~0x7f;
    if (Offset > 64) Offset -= 64;
    TtyPutLn(); Dump( FlashAddress+Offset, FlashBuffer+Offset, 256);
    TtyPutLn();
    return FLASH_ERROR;
  }
  return FLASH_OK;
}

// Verify FlashBytes (a multiple of JF.PageSize) from flash at address FlashAddress (also
// a multiple of JF.PageSize) match the default buffer at ADDRESS(BOARD_SDRAM_BASE).

FLASH_Status JF_Verify( bit32 FlashAddress, bit32 FlashBytes, bit8 Trace) {
  bit32 Offset;
  bptr WriteBuffer = ADDRESS(BOARD_SDRAM_BASE);
  bptr FlashBuffer = ADDRESS(BOARD_SDRAM_BASE) + MB(16);
  if ( FlashAddress               % JF.BlockSize ) return FLASH_ERROR;
  if ( FlashBytes                 % JF.BlockSize ) return FLASH_ERROR;
  if ((FlashAddress + FlashBytes) > JF.TotalBytes) return FLASH_MEMORY_OVERFLOW;
  if (JF_Read( FlashAddress, FlashBytes, FlashBuffer, Trace ? 256 : 0) != FLASH_OK) return FLASH_ERROR; 
  if (Compare( WriteBuffer, FlashBuffer, FlashBytes, &Offset)) {
    TtyPutStr( "Verify failed at $");
    TtyPutHex( FlashAddress+Offset, 6);
    TtyPutLn();
    Offset &= ~0x7f;
    if (Offset > 64) Offset -= 64;
    TtyPutLn(); Dump( FlashAddress+Offset, WriteBuffer+Offset, 256);
    TtyPutLn(); Dump( FlashAddress+Offset, FlashBuffer+Offset, 256);
    TtyPutLn();
    return FLASH_ERROR;
  }
  return FLASH_OK;
}

// Write FlashBytes (a multiple of JF.BlockSize) to flash at address FlashAddress (also a
// multiple of JF.BlockSize) from the default buffer at ADDRESS(BOARD_SDRAM_BASE).  We need to
// write a temporary copy of the data because it will be erased in-place by the low-level
// spi write operation.  If TraceBytes is nonzero, output a progress indicator as each
// block is written.

FLASH_Status JF_Write( bit32 FlashAddress, bit32 FlashBytes, bit8 Trace) {
  bit32 TraceKBytes = JF.BlockSize / 1024;
  bptr WriteBuffer = ADDRESS(BOARD_SDRAM_BASE);
  bptr FlashBuffer = ADDRESS(BOARD_SDRAM_BASE) + MB(16);
  MemCpy( FlashBuffer, WriteBuffer, FlashBytes);
  SPI_SpiInit( BOARD_FLASH_CS_JEDEC, &JF.Spi);
  while (FlashBytes) {
    if (JF_BlockWrite( FlashAddress, FlashBuffer) != FLASH_OK) return FLASH_ERROR;
    if (Trace) {
      TtyPutDec( TraceKBytes, 1);
      TtyPutStr( "...");
    }
    FlashBytes   -= JF.BlockSize;
    FlashAddress += JF.BlockSize;
    FlashBuffer  += JF.BlockSize;
  }
  return FLASH_OK;
}

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

