//-------------------------------------------------------------------------------------------
//  clock.c
//-------------------------------------------------------------------------------------------

#include "main.h"

// Terminology:
//
//        Slow Clock = 32 KHz XTAL/RC oscillator
//        Main Clock = 12 MHz XTAL/RC oscillator
//      Master Clock = 48/133 Mhz (before and after ClockConfig)
//   Processor Clock = 96/400 Mhz (before and after ClockConfig)

//#define MASTER_CLOCK_133  // uncomment for faster 133 MHz master clock (vs. 100 MHz)

// The Master Clock determines how much stress we put on SDRAM memory access.  Although it
// should run at 133 MHz we seem to have problems trying to push it that fast.  The Processor
// clock should be good to 400 MHz.

// To generate desired Master and Processor Clocks, we start by programming PLLA for a multiple
// of the 12 MHz Main Clock as follows:
//
//    PLLA = 12 MHz * MULA / PLLA_DIVA
//                     ||     ||
//                     ||    PLLA_DIVA can range from 1 to 255
//                     ||
//                    MULA = PLLA_MULA+1, and can range from 2 to 255
//
// There are other aspects such as charge-pump configuration which must also be set properly
// depending on the final frequency of PLLA.  We assume multiplier 200 and divisor 3 for an
// 800 MHz result.

// Next, we divide PLLA to get the Processor Clock as follows:
//
//    Processor Clock = PLLA / PLLADIV2 / PRES
//
// Since PLLADIV2 and PRES give a power-of-two divisor, processor clock can be 400, 200,
// 100 etc.  We then divide the Processor Clock by 2, 3, or 4 to get Master Clock.
//
//    Master Clock = Processor Clock / MDIV

bit32      SlowClock =    32768;
bit32      MainClock = 12000000;
bit32    MasterClock = 48000000;
bit32 ProcessorClock = 96000000;

bit8 SlowClockFlag = 0; // 0=Internal, 1=External
bit8 FastClockFlag = 0; // RomBOOT starts us out at 48/96 MHz

#define FAST_PLLA_MULA  (199 << 16)
#define FAST_PLLA_COUNT ( 63 <<  8)

#define SLOW_PLLA_MULA  ( 63 << 16)
#define SLOW_PLLA_COUNT ( 17 <<  8)

// If fast clock not in desired mode, flip it to desired mode.  In fast mode the processor
// clock is increased to 400 MHz from its conservative 96 MHz value inherited from RomBOOT.

void FastClockConfig( bit8 FastFlag) {
  if (FastClockFlag ^ FastFlag) {

    // Assume PLLA charge pump already set up for 800 MHz.  We presume this to be the case
    // as the datasheet specifies a reset value for this register of $01000100 which has
    // the ICPLLA bit set to 0 already and the register is write-only so we can't preserve
    // the other bits (which are total mysteries).

    if (FastFlag) {
      // PLLA = MAIN=12MHz * PLLA_MULA=200 / PLLA_DIVA=3 = 800 MHz (ICPLLA=0, OUTA=0) 
      MemPut32( AT91C_PMC_PLLAR, AT91C_CKGR_SRCA | // 0x20000000  required
                                 FAST_PLLA_MULA  | // 0x00c70000  multiply by 199+1
                                 FAST_PLLA_COUNT | // 0x00003f00  wait 63 slow clocks
                                 3              ); // 0x00000003  divide by 1
    } else {
      // PLLA = MAIN=12MHz * PLLA_MULA=64 / PLLA_DIVA=1 = 768 MHz (ICPLLA=0, OUTA=0) 
      MemPut32( AT91C_PMC_PLLAR, AT91C_CKGR_SRCA | // 0x20000000  required
                                 SLOW_PLLA_MULA  | // 0x00c70000  multiply by 63+1
                                 SLOW_PLLA_COUNT | // 0x00001100  wait 17 slow clocks
                                 1              ); // 0x00000001  divide by 1
    }
    while ((MemGet32( AT91C_PMC_SR) & AT91C_PMC_LOCKA ) == 0) ; // wait for PLLA locked
    while ((MemGet32( AT91C_PMC_SR) & AT91C_PMC_MCKRDY) == 0) ; // wait for MasterClock ready

    if (FastFlag) {
      // Processor Clock = PLLA=800MHz / PRES=1 / PLLADIV2=2          = 400 MHz
      //    Master Clock = PLLA=800MHz / PRES=1 / PLLADIV2=2 / MDIV=3 = 133 MHz
      MemPut32( AT91C_PMC_MCKR, AT91C_PMC_PLLADIV2_2   | // 0x00001000  divide by 2
                                #ifdef MASTER_CLOCK_133
                                  AT91C_PMC_MDIV_3       | // 0x00000300  divide by 3
                                #else
                                  AT91C_PMC_MDIV_4       | // 0x00000200  divide by 4
                                #endif
                                AT91C_PMC_PRES_CLK     | // 0x00000000  divide by 0
                                AT91C_PMC_CSS_PLLA_CLK); // 0x00000002  select plla (800 MHz)
      while ((MemGet32( AT91C_PMC_SR) & AT91C_PMC_MCKRDY) == 0) ;
      ProcessorClock = 400000000; // 400 MHz
      #ifdef MASTER_CLOCK_133
        MasterClock = ProcessorClock / 3; // 133 MHz, MDIV = 3
      #else
        MasterClock = ProcessorClock / 4; // 100 MHz, MDIV = 4
      #endif
    } else {
      // Processor Clock = PLLA=768MHz / PRES=4 / PLLADIV2=2          = 96 MHz
      //    Master Clock = PLLA=768MHz / PRES=4 / PLLADIV2=2 / MDIV=2 = 48 MHz
      MemPut32( AT91C_PMC_MCKR, AT91C_PMC_PLLADIV2_2   | // 0x00001000  divide by 2
                                AT91C_PMC_MDIV_2       | // 0x00000100  divide by 2
                                AT91C_PMC_PRES_CLK_4   | // 0x00000000  divide by 4
                                AT91C_PMC_CSS_PLLA_CLK); // 0x00000002  select plla (768 MHz)
      while ((MemGet32( AT91C_PMC_SR) & AT91C_PMC_MCKRDY) == 0) ;
      ProcessorClock = 96000000; // 96 MHz
      MasterClock = ProcessorClock / 2; // 48 MHz, MDIV = 2
    }
    FastClockFlag = FastFlag;
} }

// Set SlowClockFlag to 0 if the internal rc oscillator is active, 1 if the external crystal
// oscillator is active.  This setting is battery-backed and hence non-volatile, so we have
// to query it early in the boot process to determine its actual state.

void SlowClockQuery( void) {
  SlowClockFlag = (MemGet32( AT91C_SYS_SLCKSEL) & AT91C_SLCKSEL_OSC32EN) ? 1 : 0;
}

// If slow clock not in desired mode, flip it to desired mode.  We can switch the slow clock
// from the internal RC oscillator to the external crystal oscillator if it isn't already
// configured that way (that setting is battery-backed and hence non-volatile).

void SlowClockConfig( bit8 SlowFlag) {
  if (SlowClockFlag ^ SlowFlag) {
    TtyPutStr( "\nSwitching to ");
    TtyPutStr( SlowFlag ? "external" : "internal");
    TtyPutStr( " 32 KHz RTC oscillator..."); DelayMs( 100);
    bit32 SLCKSEL = MemGet32( AT91C_SYS_SLCKSEL);
    if (SlowFlag) {
      SLCKSEL |=  AT91C_SLCKSEL_OSC32EN; MemPut32( AT91C_SYS_SLCKSEL, SLCKSEL); DelayMs( 1000);
      SLCKSEL |=  AT91C_SLCKSEL_OSCSEL ; MemPut32( AT91C_SYS_SLCKSEL, SLCKSEL); DelayMs(    5);
      SLCKSEL &= ~AT91C_SLCKSEL_RCEN   ; MemPut32( AT91C_SYS_SLCKSEL, SLCKSEL);
    } else {
      SLCKSEL |=  AT91C_SLCKSEL_RCEN   ; MemPut32( AT91C_SYS_SLCKSEL, SLCKSEL); DelayMs( 1000);
      SLCKSEL &= ~AT91C_SLCKSEL_OSCSEL ; MemPut32( AT91C_SYS_SLCKSEL, SLCKSEL); DelayMs(    5);
      SLCKSEL &= ~AT91C_SLCKSEL_OSC32EN; MemPut32( AT91C_SYS_SLCKSEL, SLCKSEL);
    } 
    TtyPutOk(); DelayMs( 1000);
    SlowClockFlag = SlowFlag;
} }

//  Display current clock settings.

void TtyPutClock( void) {
  TtyPutLn();
	TtyPutStr( "     "); TtyPutStrLn( BOARD_CPU_TYPE);
	TtyPutStr( "       XTAL "); TtyPutDec(      MainClock / 1000000, 0);
  TtyPutStr( " MHz, CPU "  ); TtyPutDec( ProcessorClock / 1000000, 0);
  TtyPutStr( " MHz, MCLK " ); TtyPutDec(    MasterClock / 1000000, 0);
  TtyPutStr( " MHz, SCLK 32 KHz ");
  if (SlowClockFlag) {
    TtyPutStrLn( "External");
  } else {
    TtyPutStrLn( "Internal");
} }

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

