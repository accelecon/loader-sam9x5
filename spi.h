//-------------------------------------------------------------------------------------------
//  spi.h
//-------------------------------------------------------------------------------------------

typedef struct {
	bit8  Command[8]; // align on 32-bit
  bit32 ChipSelect; // align on 32-bit
	bit32 DataSize  ; // align on 32-bit
	bptr  Data      ; // align on 32-bit
} SpiData;

#define	SPI_ERROR 0x01
#define SPI_OK    0x02

typedef bit8 SPI_Status   ;

void       SPI_SpiInit     ( bit32 ChipSelect, SpiData * Spi);
SPI_Status SPI_SpiReadWrite( bit32 CmdSize   , SpiData * Spi);

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

