//-------------------------------------------------------------------------------------------
//  register.c
//-------------------------------------------------------------------------------------------

#include "main.h"

typedef struct {
  bit32 Addr;
  ccptr Name;
} a_Register;

static a_Register RegisterList[] = {
  { 0xfffff110, "AIC_IMR"    },
  { 0xfffffe50, "SCKC_CR"    },
  { 0xfffffc08, "PMC_SCSR"   },
  { 0xfffffc18, "PMC_PCSR"   },
  { 0xfffffc1c, "CKGR_UCKR"  },
  { 0xfffffc20, "CKGR_MOR"   },
  { 0xfffffc24, "CKGR_MCFR"  },
  { 0xfffffc28, "CKGR_PLLAR" },
  { 0xfffffc30, "PMC_MCKR"   },
  { 0xfffffc38, "PMC_USB"    },
  { 0xfffffc3c, "PMC_SMD"    },
  { 0xfffffc40, "PMC_PCK0"   },
  { 0xfffffc44, "PMC_PCK1"   },
  { 0xfffffc68, "PMC_SR"     },
  { 0xfffffc6c, "PMC_IMR"    },
  { 0xfffffce4, "PMC_WPMR"   },
  { 0xfffffce8, "PMC_WPSR"   },
  { 0xfffffd0c, "PMC_PCR"    },
  { 0, 0 }
};

void TtyPutRegisters( void) {
  bit32 i;
  TtyPutLn();
  for (i = 0; RegisterList[i].Addr; i++) {
    TtyPutStr( "  $"); TtyPutHex( MemGet32( RegisterList[i].Addr), 8);
    TtyPutStr( "  "); TtyPutStrLn( RegisterList[i].Name);
} }

//-------------------------------------------------------------------------------------------
// end
//-------------------------------------------------------------------------------------------

