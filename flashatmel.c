//-------------------------------------------------------------------------------------------
//  flashatmel.c - Atmel AT45DB-series SPI AtmelFlash support
//-------------------------------------------------------------------------------------------

#include "main.h"

const cptr AtmelFlash = "Atmel";

// Flash interface data.

static struct {
  SpiData Spi          ;
  bit8    TypeCode     ;
  bit8    Status       ;
  bit32   PageCount    ;
  bit32   PageSize     ;
  bit32   PageShiftBits;
  bit32   BlockSize    ; // multiple of PageSize less than 2^15
  bit32   AddressMask  ;
  bit32   TotalBytes   ;
} AF;

#define AT45DB011  0x0c  //   1 Mb part
#define AT45DB021  0x14  //   2 Mb part
#define AT45DB161  0x2c  //  16 Mb part
#define AT45DB321  0x34  //  32 Mb part
#define AT45DB642  0x3c  //  64 Mb part
#define AT45DB128  0x10  // 128 Mb part

#define OPCODE_PAGE_ERASE       0x81  // page erase
#define OPCODE_BUF1_WRITE       0x84  // buffer 1 write
#define OPCODE_BUF1_PAGE_PGM    0x88  // buffer 1 to main memory page program without built-In erase
#define OPCODE_STATUS           0xd7  // status register read
#define OPCODE_CONTINUOUS_READ  0xe8  // continuous array read

#define STATUS_READY     0x80  // ready
#define STATUS_COMP      0x40  // compare error
#define STATUS_AT45DB    0x3c  // chip type
#define STATUS_PROTECT   0x02  // protect
#define STATUS_PAGESIZE  0x01  // page size power of 2

// Wait for any write or erase operation to complete.  As a side effect,
// update the AF.Status value.

static FLASH_Status AF_WaitReady( void) {
  bit32 Timeout = MasterClock / 1200;
  AF.Status = 0;
  AF.Spi.DataSize = 0;
  do {
    AF.Spi.Command[0] = OPCODE_STATUS;
    AF.Spi.Command[1] = 0;
    if (SPI_SpiReadWrite( 2, &AF.Spi) == SPI_OK) {
      AF.Status = AF.Spi.Command[1];
    }
    Timeout--;
  } while (((AF.Status & STATUS_READY) != STATUS_READY) && Timeout);
  return AF.Status & STATUS_READY ? FLASH_OK : FLASH_ERROR;
}

// Send an SPI command which carries an address.  The command format
// depends on the number of pages supported by the flash chip.

static FLASH_Status AF_SpiReadWriteBuffer( bit8 OpCode, bit32 CmdSize, bit32 FlashAddress, bit32 ReadWriteBytes) {
  AF_WaitReady();
  bit32 Address = ((FlashAddress / AF.PageSize) << AF.PageShiftBits) + (FlashAddress % AF.PageSize);
  AF.Spi.DataSize = ReadWriteBytes;
  AF.Spi.Command[0] = OpCode;
  if (AF.PageCount >= 16384) {
    AF.Spi.Command[1] = (bit8)((Address & 0x0F000000) >> 24);
    AF.Spi.Command[2] = (bit8)((Address & 0x00FF0000) >> 16);
    AF.Spi.Command[3] = (bit8)((Address & 0x0000FF00) >>  8);
    AF.Spi.Command[4] = (bit8)( Address & 0x000000FF);
  } else {
    AF.Spi.Command[1] = (bit8)((Address & 0x00FF0000) >> 16);
    AF.Spi.Command[2] = (bit8)((Address & 0x0000FF00) >>  8);
    AF.Spi.Command[3] = (bit8)( Address & 0x000000FF)      ;
    AF.Spi.Command[4] = 0;
  }
  AF.Spi.Command[5] = OpCode;
  AF.Spi.Command[6] = OpCode;
  AF.Spi.Command[7] = OpCode;
  return (SPI_SpiReadWrite( CmdSize, &AF.Spi) == SPI_OK) ? FLASH_OK : FLASH_ERROR;
}

// Read any number of bytes from any address on the flash chip.

static FLASH_Status AF_ContinuousRead( bit32 FlashAddress, bptr ReadBuffer, bit32 ReadBytes) {
  if ((FlashAddress + ReadBytes) > AF.TotalBytes) return FLASH_MEMORY_OVERFLOW;
  AF.Spi.Data = ReadBuffer;
  return AF_SpiReadWriteBuffer( OPCODE_CONTINUOUS_READ, 8, FlashAddress, ReadBytes);
}

// Return the command length (4 or 5) depending on the number of
// pages supported by the flash chip.

static bit32 AF_CmdSize( void) {
  return (AF.PageCount >= 16384) ? 5 : 4;
}

// Buffer a page of data in the flash chip in preparation for an erase/write
// sequence.

static FLASH_Status AF_PageCopy( bptr Buffer, bit32 PageOffset, bit32 PageBytes) {
  AF_WaitReady();
  if (PageOffset > AF.PageSize) return FLASH_BAD_ADDRESS;
  AF.Spi.Command[0] = OPCODE_BUF1_WRITE;
  AF.Spi.Command[1] = 0;
  if (AF.PageCount >= 16384) {
    AF.Spi.Command[2] = 0;
    AF.Spi.Command[3] = (bit8)(((bit32)(PageOffset & AF.AddressMask)) >> 8);
    AF.Spi.Command[4] = (bit8)( (bit32) PageOffset & 0x00FF               );
  } else {
    AF.Spi.Command[2] = (bit8)(((bit32)(PageOffset & AF.AddressMask)) >> 8);
    AF.Spi.Command[3] = (bit8)( (bit32) PageOffset & 0x00FF               );
    AF.Spi.Command[4] = 0;
  }
  AF.Spi.Data     = Buffer   ;
  AF.Spi.DataSize = PageBytes;
  return (SPI_SpiReadWrite( AF_CmdSize(), &AF.Spi) == SPI_OK) ? FLASH_OK : FLASH_ERROR;
}

// Identify the flash device and prepare it for use.

FLASH_Status AF_Init( bit32 PageSize) {
  SPI_SpiInit( BOARD_FLASH_CS_ATMEL, &AF.Spi);
  AF_WaitReady();
  if (AF.Status & 1) {
    return FLASH_BAD_PAGESIZE;
  }
  switch (AF.TypeCode = (AF.Status == 0xff) ? 0 : AF.Status & 0x3C) {
    case AT45DB011: AF.PageCount =   512; AF.PageSize =  264; AF.PageShiftBits =  9; AF.AddressMask = 0x0100; break;
    case AT45DB021: AF.PageCount =   512; AF.PageSize =  264; AF.PageShiftBits =  9; AF.AddressMask = 0x0100; break;
    case AT45DB161: AF.PageCount =  4096; AF.PageSize =  528; AF.PageShiftBits = 10; AF.AddressMask = 0x0300; break;
    case AT45DB321: AF.PageCount =  8192; AF.PageSize =  528; AF.PageShiftBits = 10; AF.AddressMask = 0x0300; break;
    case AT45DB642: AF.PageCount =  8192; AF.PageSize = 1056; AF.PageShiftBits = 11; AF.AddressMask = 0x0700; break;
    case AT45DB128: AF.PageCount = 16384; AF.PageSize = 1056; AF.PageShiftBits = 11; AF.AddressMask = 0x0700; break;
    default: return FLASH_ERROR;
  }
  AF.BlockSize  = (0x7FFF / AF.PageSize) * AF.PageSize;
  AF.TotalBytes =           AF.PageCount * AF.PageSize;
  return AF.PageSize == PageSize ? FLASH_OK : FLASH_ERROR;
}

// Print the flash device identifier (internal helper routine).

static void AF_PrintDevice( cptr Type) {
  TtyPutStr( "     Atmel AT45DB");
  TtyPutStr( Type);
}

// Print the flash device identifier.

void AF_Print( void) {
  switch (AF.TypeCode) {
    case AT45DB011: AF_PrintDevice( "011"); break;
    case AT45DB021: AF_PrintDevice( "021"); break;
    case AT45DB161: AF_PrintDevice( "161"); break;
    case AT45DB321: AF_PrintDevice( "321"); break; 
    case AT45DB642: AF_PrintDevice( "642"); break;
    case AT45DB128: AF_PrintDevice( "128"); break;
} }

// Verify that FlashBytes of flash at address FlashAddress are
// clear (erased to 0xff).

FLASH_Status AF_Clear( bit32 FlashAddress, bit32 FlashBytes) {
  bptr FlashBuffer = ADDRESS(BOARD_SDRAM_BASE) + MB(12);
  if ((FlashAddress + FlashBytes) > AF.TotalBytes) return FLASH_MEMORY_OVERFLOW;
  SPI_SpiInit( BOARD_FLASH_CS_ATMEL, &AF.Spi);
  while (FlashBytes) {
    bit32 Offset, ReadBytes = (FlashBytes > AF.BlockSize) ? AF.BlockSize : FlashBytes; 
    if (AF_ContinuousRead( FlashAddress, FlashBuffer, ReadBytes) != FLASH_OK) return FLASH_ERROR;
    if (AllFoxes( FlashBuffer, ReadBytes, &Offset) == 0) {
      TtyPutStr( "AllFoxes failed at $");
      TtyPutHex( FlashAddress+Offset, 6);
      TtyPutLn();
      Offset &= ~0x7f;
      if (Offset > 64) Offset -= 64;
      TtyPutLn(); Dump( FlashAddress+Offset, FlashBuffer+Offset, 256);
      TtyPutLn();
      return FLASH_ERROR;
    }
    FlashBytes   -= ReadBytes;
    FlashAddress += ReadBytes;
  }
  return FLASH_OK;
}

// Read FlashBytes from flash at address FlashAddress.

FLASH_Status AF_Read( bit32 FlashAddress, bit32 FlashBytes, bptr ReadBuffer) {
  if ((FlashAddress + FlashBytes) > AF.TotalBytes) return FLASH_MEMORY_OVERFLOW;
  SPI_SpiInit( BOARD_FLASH_CS_ATMEL, &AF.Spi);
  while (FlashBytes) {
    bit32 ReadBytes = (FlashBytes > AF.BlockSize) ? AF.BlockSize : FlashBytes; 
    if (AF_ContinuousRead( FlashAddress, ReadBuffer, ReadBytes) != FLASH_OK) return FLASH_ERROR;
    FlashBytes   -= ReadBytes;
    FlashAddress += ReadBytes;
    ReadBuffer   += ReadBytes;
  }
  return FLASH_OK;
}

// Verify FlashBytes (a multiple of AF.PageSize) from flash at address FlashAddress (also
// a multiple of AF.PageSize) match the default buffer at ADDRESS(BOARD_SDRAM_BASE).

FLASH_Status AF_Verify( bit32 FlashAddress, bit32 FlashBytes) {
  bit32 Offset;
  bptr WriteBuffer = ADDRESS(BOARD_SDRAM_BASE);
  bptr FlashBuffer = ADDRESS(BOARD_SDRAM_BASE) + MB(12);
  if ( FlashAddress               % AF.PageSize  ) return FLASH_ERROR;
  if ( FlashBytes                 % AF.PageSize  ) return FLASH_ERROR;
  if ((FlashAddress + FlashBytes) > AF.TotalBytes) return FLASH_MEMORY_OVERFLOW;
  SPI_SpiInit( BOARD_FLASH_CS_ATMEL, &AF.Spi);
  while (FlashBytes) {
    bit32 ReadBytes = FlashBytes > AF.BlockSize ? AF.BlockSize : FlashBytes;
    if (AF_Read( FlashAddress, ReadBytes, FlashBuffer) != FLASH_OK) return FLASH_ERROR;
    if (Compare( WriteBuffer, FlashBuffer, ReadBytes, &Offset)) {
      TtyPutStr( "Verify failed at $");
      TtyPutHex( FlashAddress+Offset, 4);
      TtyPutLn();
      Offset &= ~0x7f;
      if (Offset > 64) Offset -= 64;
      TtyPutLn(); Dump( FlashAddress+Offset, WriteBuffer+Offset, 256);
      TtyPutLn(); Dump( FlashAddress+Offset, FlashBuffer+Offset, 256);
      TtyPutLn();
      return FLASH_ERROR;
    }
    FlashBytes   -= ReadBytes;
    FlashAddress += ReadBytes;
    WriteBuffer  += ReadBytes;
  }
  return FLASH_OK;
}

// Write FlashBytes (a multiple of AF.PageSize) to flash at address FlashAddress (also a
// multiple of AF.PageSize) from the default buffer at ADDRESS(BOARD_SDRAM_BASE).  We need to
// write a temporary copy of the data because it will be erased in-place by the low-level
// spi write operation.

FLASH_Status AF_Write( bit32 FlashAddress, bit32 FlashBytes) {
  bptr WriteBuffer = ADDRESS(BOARD_SDRAM_BASE);
  bptr FlashBuffer = ADDRESS(BOARD_SDRAM_BASE) + MB(12);
  if ( FlashAddress               %  AF.PageSize ) return FLASH_ERROR;
  if ( FlashBytes                 %  AF.PageSize ) return FLASH_ERROR;
  if ((FlashAddress + FlashBytes) > AF.TotalBytes) return FLASH_MEMORY_OVERFLOW;
  SPI_SpiInit( BOARD_FLASH_CS_ATMEL, &AF.Spi);
  while (FlashBytes) {
    MemCpy( FlashBuffer, WriteBuffer, AF.PageSize);
    if (AF_PageCopy          ( FlashBuffer, 0, AF.PageSize                        ) != FLASH_OK) return FLASH_ERROR;
    if (AF_SpiReadWriteBuffer( OPCODE_PAGE_ERASE   , AF_CmdSize(), FlashAddress, 0) != FLASH_OK) return FLASH_ERROR;
    if (AF_SpiReadWriteBuffer( OPCODE_BUF1_PAGE_PGM, AF_CmdSize(), FlashAddress, 0) != FLASH_OK) return FLASH_ERROR;
    FlashBytes   -= AF.PageSize;
    FlashAddress += AF.PageSize;
    WriteBuffer  += AF.PageSize;
    FlashBuffer  += AF.PageSize;
  }
  return FLASH_OK;
}

// Erase FlashBytes (a multiple of AF.PageSize) in flash at address FlashAddress (also a
// multiple of AF.PageSize).

FLASH_Status AF_Erase( bit32 FlashAddress, bit32 FlashBytes) {
  if ( FlashAddress               %  AF.PageSize ) return FLASH_ERROR;
  if ( FlashBytes                 %  AF.PageSize ) return FLASH_ERROR;
  if ((FlashAddress + FlashBytes) > AF.TotalBytes) return FLASH_MEMORY_OVERFLOW;
  SPI_SpiInit( BOARD_FLASH_CS_ATMEL, &AF.Spi);
  while (FlashBytes) {
    if (AF_SpiReadWriteBuffer( OPCODE_PAGE_ERASE, AF_CmdSize(), FlashAddress, 0) != FLASH_OK) return FLASH_ERROR;
    FlashBytes   -= AF.PageSize;
    FlashAddress += AF.PageSize;
  }
  return FLASH_OK;
}

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

