//-------------------------------------------------------------------------------------------
//  acnbfx100.h - constants and macros for the acnbfx100 board
//-------------------------------------------------------------------------------------------

#include "at91sam9x25.h"  // Atmel chip-specific constants

//-------------------------------------------------------------------------------------------
//  additional chip-specific constants
//-------------------------------------------------------------------------------------------

#define BOARD_CPU_TYPE        "Atmel AT91SAM9X25"

#define BOARD_SRAM_BASE       0x00300000
#define BOARD_SRAM_SIZE       KB(32)      // 32 KB

//-------------------------------------------------------------------------------------------
//  board-to-chip mappings for SPI0 --> SPI
//-------------------------------------------------------------------------------------------

#define BOARD_SPI_MOSI        (AT91C_PA12_SPI0_MOSI)
#define BOARD_SPI_MISO        (AT91C_PA11_SPI0_MISO)
#define BOARD_SPI_SPCK        (AT91C_PA13_SPI0_SPCK)
#define BOARD_SPI_NPCS0       (AT91C_PA14_SPI0_NPCS0)
#define BOARD_SPI_NPCS1       (AT91C_PA7_SPI0_NPCS1)

#define BOARD_SPI_CR          (AT91C_SPI0_CR)
#define BOARD_SPI_MR          (AT91C_SPI0_MR)
#define BOARD_SPI_SR          (AT91C_SPI0_SR)
#define BOARD_SPI_CSR         (AT91C_SPI0_CSR)
#define BOARD_SPI_TDR         (AT91C_SPI0_TDR)
#define BOARD_SPI_RDR         (AT91C_SPI0_RDR)

#define BOARD_ID_SPI          (AT91C_ID_SPI0)

//-------------------------------------------------------------------------------------------
//  board-to-chip mappings for ETH0 --> ETH
//-------------------------------------------------------------------------------------------

#define BOARD_ID_EMAC         (AT91C_ID_EMAC0)

//-------------------------------------------------------------------------------------------
//  board-to-chip mappings for UHPHS --> USB
//-------------------------------------------------------------------------------------------

#define BOARD_ID_USB          (AT91C_ID_UHPHS)

//-------------------------------------------------------------------------------------------
//  board-to-gpio mappings
//-------------------------------------------------------------------------------------------

// ethernet control and status

#define BOARD_PORTA_ETH_ENABLE  BIT( 0)   // output, active high
#define BOARD_PORTA_ETH_ACTIVE  BIT( 1)   // input, active low
#define BOARD_PORTA_ETH_LINK    BIT( 2)   // input, active low

// full-color leds for usb and 3g status

#define BOARD_PORTC_LED_USB_R   BIT( 3)   // output, pulled low, active low
#define BOARD_PORTC_LED_USB_G   BIT( 4)   // output, pulled low, active low
#define BOARD_PORTC_LED_USB_B   BIT( 5)   // output, active low

#define BOARD_PORTC_LED_3G_R    BIT( 6)   // output, active low
#define BOARD_PORTC_LED_3G_G    BIT( 7)   // output, active low
#define BOARD_PORTC_LED_3G_B    BIT( 8)   // output, active low

// simple green leds for signal strength bar

#define BOARD_PORTC_LED_SIG_1   BIT( 9)   // output, active high
#define BOARD_PORTC_LED_SIG_2   BIT(10)   // output, active high
#define BOARD_PORTC_LED_SIG_3   BIT(11)   // output, active high
#define BOARD_PORTC_LED_SIG_4   BIT(12)   // output, active high
#define BOARD_PORTC_LED_SIG_5   BIT(13)   // output, active high

// tact switch labeled 'default'

#define BOARD_PORTD_SW1         BIT( 0)   // input, requires pullup, active low

// internal and external usb port power control and monitoring

#define BOARD_PORTD_USBI_DIS    BIT( 4)   // output, pulled low, low to enable usb power
#define BOARD_PORTD_USBX_DIS    BIT( 5)   // output, pulled low, low to enable usb power
#define BOARD_PORTD_USBI_FLG    BIT( 8)   // input, pulled high, overcurrent detect
#define BOARD_PORTD_USBX_FLG    BIT( 9)   // input, pulled high, overcurrent detect

//-------------------------------------------------------------------------------------------
//  general board architecture
//
//    SDRAM -  32 MB - NCS1  - Micron MT48LC16M16A2
//    FLASH - 128 KB - NPCS0 - Atmel AT45DB011 DataFlash
//    FLASH -  16 MB - NPCS1 - Numonyx M25P128 JEDEC SPI Flash
//-------------------------------------------------------------------------------------------

#define BOARD_CLOCK_XTAL      12000000                //  12.000 MHz
#define BOARD_CKGR_MCFR       0xFFFFFC24              // Clock Generator Main Clock Frequency Register
#define BOARD_CKGR_PLLAR      0xFFFFFC28              // Clock Generagor PLLkA Register
#define BOARD_PMC_MCKR        0xFFFFFC30              // Power Management Controller Master Clock Register
#define BOARD_PMC_PCK0        0xFFFFFC40              // Power Management Controller Master Clock Register
#define BOARD_PMC_PCK1        0xFFFFFC44              // Power Management Controller Master Clock Register
#define BOARD_PMC_SR          0xFFFFFC68              // Power Management Controller Status Register
#define BOARD_PMC_IMR         0xFFFFFC6C              // Power Management Controller Interrupt Mask Register
#define BOARD_PMC_PLLICPR     0xFFFFFC80              // Power Management Controller Charge Pump Current Register
#define BOARD_PMC_PCR         0xFFFFFD0C              // Power Management Controller Peripheral Control Register
#define BOARD_MATRIX_BASE     0xFFFFDE00              // Bus Matrix Registers Base

#define BOARD_SDRAM_BASE      0x20000000              // NCS1
#define BOARD_SDRAM_UBOOT     0x20700000              // BOARD_SDRAM_BASE+MB(7)
#define BOARD_SDRAM_LINUX_OFF 0x8000
#define BOARD_SDRAM_LINUX     BOARD_SDRAM_BASE+BOARD_SDRAM_LINUX_OFF
#define BOARD_SDRAM_SIZE      0x02000000              // 32 MB

#define BOARD_FLASH_CS_ATMEL  BOARD_SPI_NPCS0         // Atmel AT45DB011
#define BOARD_FLASH_CS_JEDEC  BOARD_SPI_NPCS1         // Numonyx M25P128

#define MACH_ACFX100          3870
//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

