//-------------------------------------------------------------------------------------------
//  clock.h
//-------------------------------------------------------------------------------------------

// slow clock status and control

extern bit8 SlowClockFlag; // 0=Internal, 1=External

void SlowClockQuery( void); // sets SlowClockFlag
void SlowClockConfig( bit8 SlowFlag); // 0=Internal, 1=External

// fast clock status and control

extern bit8 FastClockFlag; // 0=48/96 MHz, 1=133/400 MHz

void FastClockConfig( bit8 FastFlag); // sets FastClockFlag

// display clock settings

void TtyPutClock( void);

// current clock settings

extern bit32      SlowClock; // 32 KHz RC/XTAL
extern bit32      MainClock; // 12 MHz RC/XTAL
extern bit32    MasterClock; // 48/133 MHz before/after ClockConfig()
extern bit32 ProcessorClock; // 96/400 MHz before/after ClockConfig()

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

