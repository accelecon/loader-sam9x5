//-------------------------------------------------------------------------------------------
//  util.h
//-------------------------------------------------------------------------------------------

#include "macros.h"
#include "types.h"

static void inline MemPut32( bit32 Address, bit32 Data) {
  volatile bit32 * Memory = (bit32 *) Address;
  *Memory = Data;
}

static bit32 inline MemGet32( bit32 Address) {
  volatile bit32 * Memory = (bit32 *) Address;
  return *Memory;
}

extern const cptr StrAtmel;
extern const cptr StrJedec;
extern const cptr StrInit;
extern const cptr StrRead;

void Hang( ccptr Message);
void Reset( void);
void DelayRaw( bit32 Count);
void DelayMs( bit32 Milliseconds);

bit8  AllFoxes( cbptr Buffer                , bit32 ByteCount, bit32 * Offset);
bit8  Compare ( cbptr Buffer1, cbptr Buffer2, bit32 ByteCount, bit32 * Offset);
void  MemFill (  bptr Target ,  bit8 Source , bit32 ByteCount);
void  MemCpy  (  bptr Target , cbptr Source , bit32 ByteCount);
bit32 StrLen  ( ccptr String);

void  Dump( bit32 FlashAddress, cbptr Buffer, bit32 ByteCount);

typedef enum {
  e_None  ,
  e_Loader,
  e_Uboot ,
  e_Linux ,
} e_ImageType;

void DisplayMenuItem( char MenuSelection, char Partition, cptr BinName, cptr FlashType, bit32 FlashAddress, bit32 FlashBlocks, bit32 BlockSize, e_ImageType ImageType);
void XmodemToAtmelFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks, e_ImageType ImageType);
void XmodemToJedecFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks, e_ImageType ImageType);
void DumpAtmelFlash( void);
void DumpJedecFlash( void);
void ZapAtmelFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks);
void ZapJedecFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks);
void TestAtmelFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks);
void TestJedecFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks);
void CheckAtmelFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks, e_ImageType ImageType);
void CheckJedecFlash( cptr BinFile, bit32 FlashAddress, bit32 FlashBlocks, e_ImageType ImageType);

void DisableWatchdog( void);

bit8 TtyConfig( bit8 Fast, bit8 Announce);
bit8 TtyGetReady( void);
bit8 TtyGetChar( void);
void TtyPutChr( char c);
void TtyPutStr( ccptr s);
void TtyPutLn( void);
void TtyPutOk( void);
void TtyPutBad( void);
void TtyPutStrLn( ccptr s);
void TtyPutDec( bit32 Value, bit8 Digits);
void TtyPutDecPad( bit32 Value, bit8 Digits, char Pad);
void TtyPutHex( bit32 Value, bit8 Digits);
void TtyPutFailure( cptr Type, cptr Routine);
void TtyPutFlashGeometry( bit32 Blocks, bit32 BlockSize);

extern bit32 AtmelFlash_Offset;
extern bit32 JedecFlash_Offset;

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

