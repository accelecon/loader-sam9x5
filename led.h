//-------------------------------------------------------------------------------------------
//  led.h
//-------------------------------------------------------------------------------------------

// LED red/green/blue color mixes.

#define LED_BLACK   0
#define LED_RED     BOARD_PORTC_LED_USB_R
#define LED_GREEN                           BOARD_PORTC_LED_USB_G
#define LED_BLUE                                                    BOARD_PORTC_LED_USB_B
#define LED_YELLOW  BOARD_PORTC_LED_USB_R | BOARD_PORTC_LED_USB_G
#define LED_WHITE   BOARD_PORTC_LED_USB_R | BOARD_PORTC_LED_USB_G | BOARD_PORTC_LED_USB_B

// Color choices for USB indicator.  Hardware reset is yellow (red+green)

#define COLOR_INIT    LED_BLACK   // first change color set
#define COLOR_AUTO    LED_BLUE    // menu displayed, autoboot pending
#define COLOR_MENU    LED_GREEN   // menu displayed, autoboot canceled
#define COLOR_SWITCH  LED_RED     // default switch pressed
#define COLOR_LOAD    LED_YELLOW  // loading u-boot
#define COLOR_UBOOT   LED_GREEN   // launched u-boot
#define COLOR_LINUX   LED_GREEN   // launched u-boot

// Use to set the USB LED to a specific color.  Tri-color LED's are active low.

void UsbLedColor( bit32 Color);

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

