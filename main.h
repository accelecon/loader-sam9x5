//-------------------------------------------------------------------------------------------
//  main.h
//-------------------------------------------------------------------------------------------

#define LOADER_VERSION "Accelerated Concepts FX-Series Loader 2012.03"

typedef       unsigned char     bit8;
typedef const unsigned char *  cbptr;
typedef       unsigned char *   bptr;
typedef                char *   cptr;
typedef const          char *  ccptr;

typedef unsigned     short int bit16;
typedef unsigned      long int bit32;
typedef unsigned long long int bit64;

#include "acnbfx100.h"    // board-specific constants
#include "register.h"

#include "debug.h"
#include "util.h"
#include "led.h"
#include "time.h"
#include "bootlin.h"

#include "gpio.h"
#include "clock.h"
#include "sdram.h"
#include "image.h"

#include "spi.h"
#include "flash.h"
#include "flashatmel.h"
#include "flashjedec.h"

#include <string.h>

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

