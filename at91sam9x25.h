//-------------------------------------------------------------------------------------------
//  at91sam9x25.h - Constants specific to the at91sam9x25 chip architecture extracted
//                    from the file AT91SAM9GX5_inc.h create by Atmel
//-------------------------------------------------------------------------------------------

#define AT91C_MATRIX_MRCR         (0xFFFFDF00)  // (MATRIX) Master Remap Control Register
#define AT91C_MATRIX_RCA926I      (0x1UL << 0)  // (MATRIX) Remap Command Bit for ARM926EJ-S Instruction
#define AT91C_MATRIX_RCA926D      (0x1UL << 1)  // (MATRIX) Remap Command Bit for ARM926EJ-S Data

#define AT91C_US_RXRDY            (0x1UL << 0)  // (USART) RXRDY Interrupt
#define AT91C_US_TXRDY            (0x1UL << 1)  // (USART) TXRDY Interrupt

#define AT91C_AIC_IDCR            (0xFFFFF124)  // (AIC) Interrupt Disable Command Register

#define AT91C_DBGU_CSR            (0xFFFFF214)  // (DBGU) Channel Status Register
#define AT91C_DBGU_RHR            (0xFFFFF218)  // (DBGU) Receiver Holding Register
#define AT91C_DBGU_THR            (0xFFFFF21C)  // (DBGU) Transmitter Holding Register

#define AT91C_PMC_DDR             (0x1UL << 2)  // (PMC) DDR controller Clock2x

#define AT91C_PMC_SCER            (0xFFFFFC00)  // (PMC) System Clock Enable Register

#define AT91C_CCFG_EBICSA         (0xFFFFDF20)  // (CCFG)  EBI Chip Select Assignement Register

#define AT91C_EBI_CS1A_SDRAMC     (0x1UL <<  1) // (CCFG) Chip Select 1 is assigned to the SDRAM Controller.

#define AT91C_DDR2C_MDR           (0xFFFFE820)  // (DDR2C) Memory Device Register
#define AT91C_DDR2C_CR            (0xFFFFE808)  // (DDR2C) Configuration Register
#define AT91C_DDR2C_T0PR          (0xFFFFE80C)  // (DDR2C) Timing0 Register
#define AT91C_DDR2C_T1PR          (0xFFFFE810)  // (DDR2C) Timing1 Register
#define AT91C_DDR2C_RTR           (0xFFFFE804)  // (DDR2C) Refresh Timer Register
#define AT91C_DDR2C_MR            (0xFFFFE800)  // (DDR2C) Mode Register

#define AT91C_PIO_PA7             (1UL <<  7)   // Pin Controlled by PA7
#define AT91C_PIO_PA9             (1UL <<  9)   // Pin Controlled by PA9
#define AT91C_PIO_PA10            (1UL << 10)   // Pin Controlled by PA10
#define AT91C_PIO_PA11            (1UL << 11)   // Pin Controlled by PA11
#define AT91C_PIO_PA12            (1UL << 12)   // Pin Controlled by PA12
#define AT91C_PIO_PA13            (1UL << 13)   // Pin Controlled by PA13
#define AT91C_PIO_PA14            (1UL << 14)   // Pin Controlled by PA14

#define AT91C_PA7_SPI0_NPCS1      (AT91C_PIO_PA7)       //  SPI 0 Peripheral Chip Select 1
#define AT91C_PA9_DRXD            (AT91C_PIO_PA9)       //  DEBUG Receive Data
#define AT91C_PA10_DTXD           (AT91C_PIO_PA10)      //  DEBUG Transmit Data
#define AT91C_PA11_SPI0_MISO      (AT91C_PIO_PA11)      //  SPI 0 Master In Slave Out
#define AT91C_PA12_SPI0_MOSI      (AT91C_PIO_PA12)      //  SPI 0 Master Out Slave In
#define AT91C_PA13_SPI0_SPCK      (AT91C_PIO_PA13)      //  SPI 0 Serial Clock
#define AT91C_PA14_SPI0_NPCS0     (AT91C_PIO_PA14)      //  SPI 0 Peripheral Chip Select 0

#define AT91C_SPI0_CR             (0xF0000000)  // (SPI0) Control Register
  #define AT91C_SPI_SPIEN           (0x1UL <<  0) // (SPI) SPI Enable
  #define AT91C_SPI_SPIDIS          (0x1UL <<  1) // (SPI) SPI Disable
  #define AT91C_SPI_SWRST           (0x1UL <<  7) // (SPI) SPI Software reset
  #define AT91C_SPI_LASTXFER        (0x1UL << 24) // (SPI) SPI Last Transfer

#define AT91C_SPI0_MR             (0xF0000004)  // (SPI0) Mode Register
  #define AT91C_SPI_MSTR            (0x1UL <<  0) // (SPI) Master/Slave Mode
  #define AT91C_SPI_MODFDIS         (0x1UL <<  4) // (SPI) Mode Fault Detection
  #define AT91C_SPI_PCS             (0xFUL << 16) // (SPI) Peripheral Chip Select

#define AT91C_SPI0_CSR            (0xF0000030)  // (SPI0) Chip Select Register
  #define AT91C_SPI_CPOL            (0x1UL <<  0) // (SPI) Clock Polarity

#define AT91C_SPI0_SR             (0xF0000010)  // (SPI0) Status Register
  #define AT91C_SPI_RDRF            (0x1UL <<  0) // (SPI) Receive Data Register Full
  #define AT91C_SPI_TDRE            (0x1UL <<  1) // (SPI) Transmit Data Register Empty
  #define AT91C_SPI_TXEMPTY         (0x1UL <<  9) // (SPI) TXEMPTY Interrupt

#define AT91C_SPI0_TDR            (0xF000000C)  // (SPI0) Transmit Data Register
#define AT91C_SPI0_RDR            (0xF0000008)  // (SPI0) Receive Data Register

#define AT91C_PIOA_PER            (0xFFFFF400)  // (PIOA) PIO Enable Register
#define AT91C_PIOA_PDR            (0xFFFFF404)  // (PIOA) PIO Disable Register
#define AT91C_PIOA_OER            (0xFFFFF410)  // (PIOA) Output Enable Register
#define AT91C_PIOA_ODR            (0xFFFFF414)  // (PIOA) Output Disable Register
#define AT91C_PIOA_IFDR           (0xFFFFF424)  // (PIOA) Input Filter Disable Register
#define AT91C_PIOA_SODR           (0xFFFFF430)  // (PIOA) Set Output Data Register
#define AT91C_PIOA_CODR           (0xFFFFF434)  // (PIOA) Clear Output Data Register
#define AT91C_PIOA_PDSR           (0xFFFFF43C)  // (PIOA) Pin Data Status Register
#define AT91C_PIOA_IDR            (0xFFFFF444)  // (PIOA) Interrupt Disable Register
#define AT91C_PIOA_MDDR           (0xFFFFF454)  // (PIOA) Multi-driver Disable Register
#define AT91C_PIOA_PPUDR          (0xFFFFF460)  // (PIOA) Pull-up Disable Register
#define AT91C_PIOA_PPUER          (0xFFFFF464)  // (PIOA) Pull-up Enable Register
#define AT91C_PIOA_SP1            (0xFFFFF470)  // (PIOA) Select Peripheral 1 Register
#define AT91C_PIOA_SP2            (0xFFFFF474)  // (PIOA) Select Peripheral 2 Register
#define AT91C_PIOA_PPDDR          (0xFFFFF490)  // (PIOA) Pull-down Disable Register
#define AT91C_PIOA_PPDER          (0xFFFFF494)  // (PIOA) Pull-down Enable Register

#define AT91C_PMC_PCER            (0xFFFFFC10)  // (PMC) Peripheral Clock Enable Register  (0:31 PERI_ID)
  #define AT91C_ID_FIQ              ( 0)  // Advanced Interrupt Controller (FIQ)
  #define AT91C_ID_SYS              ( 1)  // System Controller
  #define AT91C_ID_PIOA_B           ( 2)  // Parallel IO Controller A and B
  #define AT91C_ID_PIOC_D           ( 3)  // Parallel IO Controller C and D
  #define AT91C_ID_SSD              ( 4)  // Software Modem
  #define AT91C_ID_US0              ( 5)  // USART 0
  #define AT91C_ID_US1              ( 6)  // USART 1
  #define AT91C_ID_US2              ( 7)  // USART 2
  #define AT91C_ID_US3              ( 8)  // USART 3
  #define AT91C_ID_TWI0             ( 9)  // TWI 0
  #define AT91C_ID_TWI1             (10)  // TWI 1
  #define AT91C_ID_TWI2             (11)  // TWI 2
  #define AT91C_ID_MCI0             (12)  // Multimedia Card Interface 0
  #define AT91C_ID_SPI0             (13)  // Serial Peripheral Interface 0
  #define AT91C_ID_SPI1             (14)  // Serial Peripheral Interface 1
  #define AT91C_ID_UART0            (15)  // UART 0
  #define AT91C_ID_UART1            (16)  // UART 1
  #define AT91C_ID_TC               (17)  // Timer Counter 0, 1, 2, 3, 4, 5
  #define AT91C_ID_PWMC             (18)  // Pulse Width Modulation Controller
  #define AT91C_ID_TSADC            (19)  // Touch Screen Controller
  #define AT91C_ID_HDMA0            (20)  // DMA Controller 0
  #define AT91C_ID_HDMA1            (21)  // DMA Controller 1
  #define AT91C_ID_HDMA             (20)  // DMA Controller 0
  #define AT91C_ID_UHPHS            (22)  // USB Host High Speed
  #define AT91C_ID_UDPHS            (23)  // USB Device HS
  #define AT91C_ID_EMAC0            (24)  // Ethernet MAC 0
  #define AT91C_ID_LCDC_ISI         (25)  // LCD Controller or Image Sensor Interface
  #define AT91C_ID_MCI1             (26)  // Multimedia Card Interface 1
  #define AT91C_ID_EMAC1            (27)  // Ethernet MAC 1
  #define AT91C_ID_SSC              (28)  // Serial Synchronous Controller
  #define AT91C_ID_CAN0             (29)  // CAN 0
  #define AT91C_ID_CAN1             (30)  // CAN 1
  #define AT91C_ID_IRQ              (31)  // Advanced Interrupt Controller (IRQ)

#define AT91C_PMC_PLLAR           (0xFFFFFC28)    // (PMC) PLL A Register
  #define AT91C_CKGR_DIVA           (0xFFUL  <<  0) // (PMC) Divider A Selected
    #define AT91C_CKGR_DIVA_0         (0x0UL)         // (PMC) Divider A output is 0
    #define AT91C_CKGR_DIVA_BYPASS    (0x1UL)         // (PMC) Divider A is bypassed
  #define AT91C_CKGR_PLLACOUNT      (0x3FUL  <<  8) // (PMC) PLL A Counter
  #define AT91C_CKGR_OUTA           (0x3UL   << 14) // (PMC) PLL A Output Frequency Range
    #define AT91C_CKGR_OUTA_0         (0x0UL   << 14) // (PMC) Please refer to the PLLA datasheet
    #define AT91C_CKGR_OUTA_1         (0x1UL   << 14) // (PMC) Please refer to the PLLA datasheet
    #define AT91C_CKGR_OUTA_2         (0x2UL   << 14) // (PMC) Please refer to the PLLA datasheet
    #define AT91C_CKGR_OUTA_3         (0x3UL   << 14) // (PMC) Please refer to the PLLA datasheet
  #define AT91C_CKGR_MULA           (0x7FFUL << 16) // (PMC) PLL A Multiplier
  #define AT91C_CKGR_SRCA           (0x1UL   << 29) // (PMC) Required For All Writes

#define AT91C_PMC_MCKR            (0xFFFFFC30)  // (PMC) Master Clock Register
  #define AT91C_PMC_CSS             (0x7UL <<  0) // (PMC) Programmable Clock Selection
    #define AT91C_PMC_CSS_SLOW_CLK    (0x0UL)       // (PMC) Slow Clock is selected
    #define AT91C_PMC_CSS_MAIN_CLK    (0x1UL)       // (PMC) Main Clock is selected
    #define AT91C_PMC_CSS_PLLA_CLK    (0x2UL)       // (PMC) Clock from PLL A is selected
    #define AT91C_PMC_CSS_UPLL_CLK    (0x3UL)       // (PMC) Clock from UTMI PLL is selected
    #define AT91C_PMC_CSS_SYS_CLK     (0x4UL)       // (PMC) System clock is selected
  #define AT91C_PMC_PRES            (0xFUL <<  4) // (PMC) Programmable Clock Prescaler
    #define AT91C_PMC_PRES_CLK        (0x0UL <<  4) // (PMC) Selected clock
    #define AT91C_PMC_PRES_CLK_2      (0x1UL <<  4) // (PMC) Selected clock divided by 2
    #define AT91C_PMC_PRES_CLK_4      (0x2UL <<  4) // (PMC) Selected clock divided by 4
    #define AT91C_PMC_PRES_CLK_8      (0x3UL <<  4) // (PMC) Selected clock divided by 8
    #define AT91C_PMC_PRES_CLK_16     (0x4UL <<  4) // (PMC) Selected clock divided by 16
    #define AT91C_PMC_PRES_CLK_32     (0x5UL <<  4) // (PMC) Selected clock divided by 32
    #define AT91C_PMC_PRES_CLK_64     (0x6UL <<  4) // (PMC) Selected clock divided by 64
    #define AT91C_PMC_PRES_CLK_3      (0x7UL <<  4) // (PMC) Selected clock divided by 3
    #define AT91C_PMC_PRES_CLK_1_5    (0x8UL <<  4) // (PMC) Selected clock divided by 1.5
  #define AT91C_PMC_MDIV            (0x3UL <<  8) // (PMC) Master Clock Division
    #define AT91C_PMC_MDIV_1          (0x0UL <<  8) // (PMC) Processor clock = Master Clock ; DDR Clock = Master Clock
    #define AT91C_PMC_MDIV_2          (0x1UL <<  8) // (PMC) Processor clock = 2 * Master Clock ; DDR Clock = 2 * Master Clock
    #define AT91C_PMC_MDIV_4          (0x2UL <<  8) // (PMC) Processor clock = 4 * Master Clock ; DDR Clock = 2 * Master Clock
    #define AT91C_PMC_MDIV_3          (0x3UL <<  8) // (PMC) Processor clock = 3 * Master Clock ; DDR Clock = 2 * Master Clock
  #define AT91C_PMC_PLLADIV2        (0x1UL << 12) // (PMC) PLLA divisor by 2
    #define AT91C_PMC_PLLADIV2_1      (0x0UL << 12) // (PMC) PLLA clock frequency is divided by 1
    #define AT91C_PMC_PLLADIV2_2      (0x1UL << 12) // (PMC) PLLA clock frequency is divided by 2

#define AT91C_PMC_SR              (0xFFFFFC68)  // (PMC) Status Register
  #define AT91C_PMC_LOCKA           (0x1UL <<  1) // (PMC) PLL A Status/Enable/Disable/Mask
  #define AT91C_PMC_MCKRDY          (0x1UL <<  3) // (PMC) Master Clock Status/Enable/Disable/Mask

#define AT91C_PMC_PLLICPR         (0xFFFFFC80)  // (PMC) PLL Charge Pump Current Register
  #define AT91C_PMC_ICPPLLA         (0xFUL << 0)  // (PMC) PLLA charge pump current setting
  #define AT91C_PMC_ICPPLLA_0         (0x0UL)       // (PMC) 595-800 MHz
  #define AT91C_PMC_ICPPLLA_1         (0x1UL)       // (PMC) 395-600 MHz
  #define AT91C_PMC_REALLOCK        (0x1UL << 7)  // (PMC) PLLs use real lock signals when 1
  #define AT91C_PMC_IPLLA           (0xFUL << 8)  // (PMC) PLLA special setting
  #define AT91C_PMC_IPLLA_0            (0x0UL << 8) // (PMC) Internal LFT
  #define AT91C_PMC_IPLLA_1            (0x1UL << 8) // (PMC) External LFT

#define AT91C_DBGU_IDR            (0xFFFFF20C)  // (DBGU) Interrupt Disable Register

#define AT91C_DBGU_BRGR           (0xFFFFF220)  // (DBGU) Baud Rate Generator Register

#define AT91C_DBGU_CR             (0xFFFFF200)  // (DBGU) Control Register
  #define AT91C_US_RSTRX            (0x1UL <<  2) // (USART) Reset Receiver
  #define AT91C_US_RSTTX            (0x1UL <<  3) // (USART) Reset Transmitter
  #define AT91C_US_RXEN             (0x1UL <<  4) // (USART) Receiver Enable
  #define AT91C_US_RXDIS            (0x1UL <<  5) // (USART) Receiver Disable
  #define AT91C_US_TXEN             (0x1UL <<  6) // (USART) Transmitter Enable
  #define AT91C_US_TXDIS            (0x1UL <<  7) // (USART) Transmitter Disable

#define AT91C_DBGU_MR             (0xFFFFF204)  // (DBGU) Mode Register
  #define AT91C_US_PAR_NONE         (0x4UL <<  9) // (USART) No Parity

#define AT91C_WDTC_WDMR           (0xFFFFFE44)  // (WDTC) Watchdog Mode Register
  #define AT91C_WDTC_WDDIS          (0x1UL << 15) // (WDTC) Watchdog Disable

#define AT91C_BASE_RSTC           (0xFFFFFE00)  // (RSTC) Base Address
  #define AT91C_RSTC_PROCRST        (0x1UL  <<  0) // (RSTC) Processor Reset
  #define AT91C_RSTC_PERRST         (0x1UL  <<  2) // (RSTC) Peripheral Reset
  #define AT91C_RSTC_KEY            (0xA5UL << 24) // (RSTC) Password

#define AT91C_SYS_SLCKSEL         (0xFFFFFE50)  // (SYS) Slow Clock Selection Register
  #define AT91C_SLCKSEL_RCEN        (0x1UL <<  0) // (SYS) Enable Internal RC Oscillator
  #define AT91C_SLCKSEL_OSC32EN     (0x1UL <<  1) // (SYS) Enable External Oscillator
  #define AT91C_SLCKSEL_OSC32BYP    (0x1UL <<  2) // (SYS) Bypass External Oscillator
  #define AT91C_SLCKSEL_OSCSEL      (0x1UL <<  3) // (SYS) External Oscillator Selection

#define AT91C_SYS_GPBR            (0xFFFFFE60)  // (SYS) General Purpose Register

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

