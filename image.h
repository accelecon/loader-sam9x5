//-------------------------------------------------------------------------------------------
//  util.h
//-------------------------------------------------------------------------------------------

typedef enum { e_Silent, e_Quiet, e_Verbose } e_DisplayLevel;

// Linux image header, all data big-endian.  Followed immediately by data.

typedef struct {
  bit32 HeaderMagic  ; // HEADER_MAGIC
  bit32 HeaderCrc    ;
  bit32 CreateTime   ;
  bit32 DataSize     ;
  bit32 DataLoad     ;
  bit32 EntryPoint   ;
  bit32 DataCrc      ;
  bit8  SystemType   ;
  bit8  ArchType     ;
  bit8  ImageType    ;
  bit8  CompType     ;
  char  ImageName[32];
} s_LinuxHeader, * p_LinuxHeader;

// For linux, we can do a quick check of the header, or a full check of the data.  Because
// the header crc calculation requires modifying the header, we also implement a header
// copy function.

p_LinuxHeader CopyLinuxHeader ( p_LinuxHeader Header);
bit8          CheckLinuxHeader( p_LinuxHeader Header, bit32 PartitionBytes, e_DisplayLevel DisplayLevel);
bit8          CheckLinuxData  ( p_LinuxHeader Header, bptr Data);
bit8          CheckLinuxImage  ( void);

//zImage header - at offset 0x24 into the image
typedef struct {
  bit32 magic;
  bit32 start_addr;
  bit32 end_addr;
} s_zImageHeader;

// Uboot image header, all data big-endian.  Followed immediately by data.

typedef struct {
  bit32 Pad1[5] ; // vectors
  bit32 DataSize; // unused vector
  bit32 Pad2[9] ; // indirect pointers
  bit32 DataCrc ; // initially HEADER_MAGIC == 0xdeadbeef
} s_UbootHeader, * p_UbootHeader;

// For u-boot, we want to do a quick check of a raw, unstamped image, or a full check of a
// stamped image (as just read from flash).  We can also stamp a raw image with a length
// and crc before writing it to flash.

bit8 ValidUbootHeader( p_UbootHeader Header, bit32 ImageBytes, bit32 PartitionBytes);
void StampUbootHeader( p_UbootHeader Header, bit32 ImageBytes);
bit8 CheckUbootImage( bit32 FlashAddress, bit32 PartitionBytes, bptr ReadBuffer, e_DisplayLevel DisplayLevel);

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

