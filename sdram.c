//-------------------------------------------------------------------------------------------
//  sdram.c
//-------------------------------------------------------------------------------------------

#include "main.h"

// Full memory test.  ByteCount can range from 1K to 64M.

bit8 SdramTest( bit32 ByteCount, bit8 Verbose) {
  bit32 i;
  if (Verbose) TtyPutStr( "\nSDRAM_Write..."); 
  for (i = 0; i < ByteCount; i += 4) {
    MemPut32( BOARD_SDRAM_BASE + i, i);
    if (Verbose && ((i % MB(1)) == 0)) {
      TtyPutChr( '#');
      DelayMs( 100);
  } }
  if (Verbose) {
    TtyPutStrLn( "...OK");
    DelayMs( 5000);
    TtyPutStr( "SDRAM_Check..."); 
  }
  for (i = 0; i < ByteCount; i += 4) {
    if (MemGet32( BOARD_SDRAM_BASE + i) != i) {
      if (Verbose) TtyPutStrLn( "...BAD");
      return 0; // failure
    }
    if (Verbose && ((i % MB(1)) == 0)) {
      TtyPutChr( '#');
      DelayMs( 100);
  } }
  if (Verbose) TtyPutStrLn( "...OK");
  return 1; // success
}

// Mode register command codes:

  #define NRM 0x00
  #define NOP 0x01
  #define ABP 0x02
  #define LMR 0x03
  #define ARF 0x04

// Configure SDRAM at the specified size and then do a trivial memory test to verify
// successful configuration.  Return 1 on success, 0 on failure.  For operation at full
// bus speed (133 MHz) the CAS latency setting is critical, and is a function of the
// exact model of the SDRAM chip installed on the board.  We are using a -6E speed
// grade which requires CAS latency 3.  If you program this wrong, the board will work
// at a reduced clock speed but fail at full speed.

void SdramConfig( bit32 ByteCount) {
  bit32 i;

  MemPut32( AT91C_PMC_SCER, AT91C_PMC_DDR);
  MemPut32( AT91C_CCFG_EBICSA, MemGet32( AT91C_CCFG_EBICSA) | AT91C_EBI_CS1A_SDRAMC);

  MemPut32( AT91C_DDR2C_MDR , 0x00000010); // 16-bit SDR-SDRAM (default)
  MemPut32( AT91C_DDR2C_CR  , 0x00007029); // 9 col bits, 13 row bits, cas latency 2, 4 banks, sequential decoding
  MemPut32( AT91C_DDR2C_T0PR, 0x20224223); // TRAS 3, TRCD 2, TWR 2, TRC 4, TRP 2, TRRD 2, TWTR 0, REDUCE_WRRD 0, TMRD 2
  MemPut32( AT91C_DDR2C_T1PR, 0x03c80408); // TRFC 8, TXSNR 4, TXSRD 200, TXP 3
  MemPut32( AT91C_DDR2C_RTR ,       1032); // Refresh Timer - 64 ms, 8192 rows, 133 MHz: (0.064*MASTER_CLOCK)/8192=1032

  // Issue first NOP and wait 200 us = 13333 (6 cycles per iteration, core at 400 MHz)
  // Issue second NOP and wait 400 ns = 27
  // Issue All Banks Precharge and wait 15 ns (TRP) = 2
  // Issue Auto Refresh and wait 66 ns (TRFC) = 5
  // Issue Load Mode Register and wait 15 ns (TMRD) = 2
  // Issue Normal

  MemPut32( AT91C_DDR2C_MR, NOP);                            MemPut32( BOARD_SDRAM_BASE     , 0); DelayRaw( 13333);
  MemPut32( AT91C_DDR2C_MR, NOP);                            MemPut32( BOARD_SDRAM_BASE     , 0); DelayRaw(    27);
  MemPut32( AT91C_DDR2C_MR, ABP);                            MemPut32( BOARD_SDRAM_BASE     , 0); DelayRaw(     2);
  MemPut32( AT91C_DDR2C_MR, ARF); for (i = 0; i < 8; i++)  { MemPut32( BOARD_SDRAM_BASE     , 0); DelayRaw(     1); }
  MemPut32( AT91C_DDR2C_MR, LMR);                            MemPut32( BOARD_SDRAM_BASE+0x20, 0); DelayRaw(     2); // burst 1, sequential, cas latency 2
  MemPut32( AT91C_DDR2C_MR, NRM);                            MemPut32( BOARD_SDRAM_BASE     , 0);

  // Now for a quick memory test...

  for (i = ByteCount; i >= MEGABYTE; i -= MEGABYTE) {
    MemPut32( BOARD_SDRAM_BASE + i - 800, i);
  }
  for (i = ByteCount; i >= MEGABYTE; i -= MEGABYTE) {
    if (MemGet32( BOARD_SDRAM_BASE + i - 800) != i) {
      Hang( "SdramConfig-Error-1");
  } }
  if (SdramTest( 1024, 0) == 0) {
    Hang( "SdramConfig-Error-2");
} }

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

