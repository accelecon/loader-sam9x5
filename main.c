//-------------------------------------------------------------------------------------------
//  main.c
//-------------------------------------------------------------------------------------------

#include "main.h"

// Some handy common strings.

static const cptr StrLoader = "loader";
static const cptr StrUboot  = "u-boot";
static const cptr StrUbEnv  = "ub-env";
static const cptr StrImage0 = "image0";
static const cptr StrImage1 = "image1";
static const cptr StrConfig = "config";

// Initialize FLASH devices and print detected configurations.

static void TtyPutConfiguration( bit8 SdramInit) {
  char DateStamp[] = __DATE__;
  if (DateStamp[4] == ' ') {
    DateStamp[4] = '0';
  }
  TtyPutStr( "\n" LOADER_VERSION " - ");
  TtyPutStr( DateStamp);
  TtyPutStrLn( " " __TIME__);
  if (SdramInit) {
    SdramConfig( BOARD_SDRAM_SIZE);
  }
  TtyPutClock();
  TtyPutStr( "\n    Micron MT48LC16M16"); TtyPutDec( BOARD_SDRAM_SIZE, 23); TtyPutStrLn( " bytes");
  if (AF_Init( AF_BLOCKSIZE) == FLASH_OK) { AF_Print(); TtyPutFlashGeometry( AF_BLOCKS_TOTAL, AF_BLOCKSIZE); }
  if (JF_Init( JF_BLOCKSIZE) == FLASH_OK) { JF_Print(); TtyPutFlashGeometry( JF_BLOCKS_TOTAL, JF_BLOCKSIZE); }
}

/*
 * disable IRQ/FIQ interrupts
 * returns true if interrupts had been enabled before we disabled them
 */
static void DisableInterrupts( void) {
  MemPut32( AT91C_AIC_IDCR, 0xffffffff);
}

// Display menu and automatically launch u-boot if not interrupted.

int main( void) {
  bit8 FastTtyFlag = 0, BootFlag = 0, AutoBoot = 1;
  DisableInterrupts();
  SlowClockQuery();
  GpioConfig();
  UsbLedColor( COLOR_INIT);
  //DisableWatchdog(); // remove this for final production system
  SlowClockConfig( 1); // assure 32 KHz clock on external crystal
  FastClockConfig( 1); // assure 133/400 MHz master/processor clocks
  TtyConfig( FastTtyFlag, 0);
  TtyPutConfiguration( 1);

  bit32 LoopCount = 0;
  char Partition = 0, DisplayMenu = 1, ShowAll = 0, Switch = 0;

  // menu keys are:
  //
  //  0-5 . . partition select, then:
  //  * . . . partition erase
  //  t . . . partition test (destructive)
  //  u . . . partition upload (xmodem)
  //  d . . . partition dump
  //  c . . . partition check (linux)
  //
  //  s . . . start u-boot
  //  m . . . test memory
  //  g . . . test gpio (leds, usb power)
  //  x . . . register dump
  //  i . . . identify flash
  //  r . . . reset (reboot)
  //  ! . . . toggle fast/slow baud
  //  ? . . . toggle full/basic menu

  while (1) { // loop forever until u-boot is started or the board is reset
    if (DisplayMenu) {
      TtyPutLn();
      DisplayMenuItem( '0', Partition, StrLoader, AtmelFlash, AF_ADDR_LOADER  , AF_BLOCKS_LOADER  , AF_BLOCKSIZE, e_None );
      DisplayMenuItem( '1', Partition, StrUboot , AtmelFlash, AF_ADDR_UBOOT   , AF_BLOCKS_UBOOT   , AF_BLOCKSIZE, e_Uboot);
      DisplayMenuItem( '2', Partition, StrUbEnv , AtmelFlash, AF_ADDR_UBOOTENV, AF_BLOCKS_UBOOTENV, AF_BLOCKSIZE, e_None );
      DisplayMenuItem( '3', Partition, StrImage0, JedecFlash, JF_ADDR_IMAGE0  , JF_BLOCKS_IMAGE0  , JF_BLOCKSIZE, e_Linux);
      DisplayMenuItem( '4', Partition, StrImage1, JedecFlash, JF_ADDR_IMAGE1  , JF_BLOCKS_IMAGE1  , JF_BLOCKSIZE, e_Linux);
      DisplayMenuItem( '5', Partition, StrConfig, JedecFlash, JF_ADDR_CONFIG  , JF_BLOCKS_CONFIG  , JF_BLOCKSIZE, e_None );
      TtyPutStrLn( "\ns: start u-boot");
      if (ShowAll) {
        TtyPutStrLn( "m: test memory");
        TtyPutStrLn( "g: test gpio (leds, usb power)");
        TtyPutStrLn( "i: identify flash");
        TtyPutStrLn( "x: register dump");
        TtyPutStrLn( "r: reset (reboot)\n");
        TtyPutStrLn( "!: toggle fast/slow baud");
        TtyPutStrLn( "?: toggle full/basic menu");
        TtyPutStrLn( "^: toggle fast/slow master clock\n");
        TtyPutStrLn( "#: toggle external/internal slow clock\n");
        TtyPutStrLn( "0-5: select partition, then:");
        TtyPutStrLn( " *: partition erase");
        TtyPutStrLn( " t: partition test (destructive)");
        TtyPutStrLn( " u: partition upload (xmodem)");
        TtyPutStrLn( " d: partition dump");
        TtyPutStrLn( " c: partition check (linux)");
      }
      DisplayMenu = 0;
      UsbLedColor( AutoBoot ? COLOR_AUTO : COLOR_MENU);
    }
    bit8 MenuSelection;
    if (TtyGetReady()) {
      MenuSelection = TtyGetChar();
      AutoBoot = 0;
      UsbLedColor( COLOR_MENU);
      DisableWatchdog();
    } else {
      MenuSelection = 0;
      if (GpioGet( GPIO_PORTD, BOARD_PORTD_SW1)) { // switch inactive
        if (Switch == 1) {
          UsbLedColor( AutoBoot ? COLOR_AUTO : COLOR_MENU);
          DelayMs( 10);
          Switch = 0;
        }
      } else {
        if (Switch == 0) {
          UsbLedColor( COLOR_SWITCH);
          DelayMs( 10);
          Switch = 1;
    } } }
    if (AutoBoot) {
      LoopCount++;
    }
    switch (MenuSelection) {
      case '0': Partition = MenuSelection; AtmelFlash_Offset = AF_ADDR_LOADER  ; break;
      case '1': Partition = MenuSelection; AtmelFlash_Offset = AF_ADDR_UBOOT   ; break;
      case '2': Partition = MenuSelection; AtmelFlash_Offset = AF_ADDR_UBOOTENV; break;
      case '3': Partition = MenuSelection; JedecFlash_Offset = JF_ADDR_IMAGE0  ; break;
      case '4': Partition = MenuSelection; JedecFlash_Offset = JF_ADDR_IMAGE1  ; break;
      case '5': Partition = MenuSelection; JedecFlash_Offset = JF_ADDR_CONFIG  ; break;
      case '*':
        switch (Partition) {
          case '0': ZapAtmelFlash( StrLoader, AF_ADDR_LOADER  , AF_BLOCKS_LOADER  ); break;
          case '1': ZapAtmelFlash( StrUboot , AF_ADDR_UBOOT   , AF_BLOCKS_UBOOT   ); break;
          case '2': ZapAtmelFlash( StrUbEnv , AF_ADDR_UBOOTENV, AF_BLOCKS_UBOOTENV); break;
          case '3': ZapJedecFlash( StrImage0, JF_ADDR_IMAGE0  , JF_BLOCKS_IMAGE0  ); break;
          case '4': ZapJedecFlash( StrImage1, JF_ADDR_IMAGE1  , JF_BLOCKS_IMAGE1  ); break;
          case '5': ZapJedecFlash( StrConfig, JF_ADDR_CONFIG  , JF_BLOCKS_CONFIG  ); break;
        }
        break; 
      case 't':
        switch (Partition) {
          case '0': TestAtmelFlash( StrLoader, AF_ADDR_LOADER  , AF_BLOCKS_LOADER  ); break;
          case '1': TestAtmelFlash( StrUboot , AF_ADDR_UBOOT   , AF_BLOCKS_UBOOT   ); break;
          case '2': TestAtmelFlash( StrUbEnv , AF_ADDR_UBOOTENV, AF_BLOCKS_UBOOTENV); break;
          case '3': TestJedecFlash( StrImage0, JF_ADDR_IMAGE0  , JF_BLOCKS_IMAGE0  ); break;
          case '4': TestJedecFlash( StrImage1, JF_ADDR_IMAGE1  , JF_BLOCKS_IMAGE1  ); break;
          case '5': TestJedecFlash( StrConfig, JF_ADDR_CONFIG  , JF_BLOCKS_CONFIG  ); break;
        }
        break;
      case 'u':
        switch (Partition) {
          case '0': XmodemToAtmelFlash( StrLoader, AF_ADDR_LOADER  , AF_BLOCKS_LOADER  , e_Loader); break;
          case '1': XmodemToAtmelFlash( StrUboot , AF_ADDR_UBOOT   , AF_BLOCKS_UBOOT   , e_Uboot ); break;
          case '2': XmodemToAtmelFlash( StrUbEnv , AF_ADDR_UBOOTENV, AF_BLOCKS_UBOOTENV, e_None  ); break;
          case '3': XmodemToJedecFlash( StrImage0, JF_ADDR_IMAGE0  , JF_BLOCKS_IMAGE0  , e_Linux ); break;
          case '4': XmodemToJedecFlash( StrImage1, JF_ADDR_IMAGE1  , JF_BLOCKS_IMAGE1  , e_Linux ); break;
          case '5': XmodemToJedecFlash( StrConfig, JF_ADDR_CONFIG  , JF_BLOCKS_CONFIG  , e_None  ); break;
        }
        break;
      case 'd':
        switch (Partition) {
          case '0': DumpAtmelFlash(); break;
          case '1': DumpAtmelFlash(); break;
          case '2': DumpAtmelFlash(); break;
          case '3': DumpJedecFlash(); break;
          case '4': DumpJedecFlash(); break;
          case '5': DumpJedecFlash(); break;
        }
        break;
      case 'c':
        switch (Partition) {
          case '1': CheckAtmelFlash( StrUboot , AF_ADDR_UBOOT , AF_BLOCKS_UBOOT , e_Uboot); break;
          case '3': CheckJedecFlash( StrImage0, JF_ADDR_IMAGE0, JF_BLOCKS_IMAGE0, e_Linux); break;
          case '4': CheckJedecFlash( StrImage1, JF_ADDR_IMAGE1, JF_BLOCKS_IMAGE1, e_Linux); break;
        }
        break;
      case 'm': SdramTest( BOARD_SDRAM_SIZE, 1); break;
      case 'i': 
        TtyPutConfiguration( 0);
        break;
      case 'g': GpioTest(); break;
      case 'x': TtyPutRegisters(); break;
      case 'r': Reset(); break;
      case '?': ShowAll = ShowAll ? 0 : 1; break;
      case 's': BootFlag = 1; break;
      case '!': FastTtyFlag = TtyConfig( FastTtyFlag ? 0 : 1, 1); MenuSelection = 0; break; // don't redisplay menu
      case '^':
        FastClockConfig( FastClockFlag ? 0 : 1);
        TtyConfig( FastTtyFlag, 0);
        TtyPutConfiguration( 0);
        break;
      case '#':
        SlowClockConfig( SlowClockFlag ? 0 : 1);
        TtyConfig( FastTtyFlag, 0);
        TtyPutConfiguration( 0);
        break;
      case '=': break; // just redisplay menu
      case ' ': AutoBoot = 0; // disable autoboot
      default: MenuSelection = 0; // ignore all other characters and don't redisplay menu
    }
    if (BootFlag || ((LoopCount > 607176) && AutoBoot)) { // 2 second delay
      LoopCount = AutoBoot = 0;
      TtyPutStr( "\nLoading Linux (");
      //TtyPutDec( AF_BLOCKS_UBOOT * AF_BLOCKSIZE, 1);
      TtyPutDec( JF_BLOCKS_IMAGE0 * JF_BLOCKSIZE, 1);
      TtyPutStrLn(" bytes)...");
      if (JF_Init( JF_BLOCKSIZE)) {
        UsbLedColor( COLOR_LOAD);

        bit32 *magic      = (void *) BOARD_SDRAM_LINUX+0x24;
        bit32 *zimg_start = (void *) BOARD_SDRAM_LINUX+0x28;
        bit32 *zimg_end   = (void *) BOARD_SDRAM_LINUX+0x2C;
        bit32 zimg_len = 0;
        // Read first 0x30 bytes so we can inspect the header
        if (JF_Read( JF_ADDR_IMAGE0,  0x30, (bptr) BOARD_SDRAM_LINUX, 0) == FLASH_OK){
          if (*magic == 0x016F2818){
            TtyPutStr("Warning: bad zImage magic number: ");
            TtyPutHex(*magic, 8);
            TtyPutStr(". expecting 0x016F2818 at offset 0x24 for ARM zImage.\n");
          }
          zimg_len = *zimg_end - *zimg_start;
          if (zimg_len > BOARD_SDRAM_SIZE - BOARD_SDRAM_LINUX_OFF){
            TtyPutStr("Warning: zImage size is larger than available memory.\n");
          }
        } else {
          TtyPutFailure( StrJedec, StrRead);
        }

        TtyPutStr("Reading ");
        TtyPutDec(zimg_len, 8);
        TtyPutStr(" bytes from image0.\n");

        //finish reading the rest of the zImage
        if (JF_Read( JF_ADDR_IMAGE0+0x30, zimg_len - 0x30, (bptr) BOARD_SDRAM_LINUX+0x30, 0) == FLASH_OK){
          if (CheckLinuxImage()) {
            TtyPutStr( "Starting Linux at 0x"); TtyPutHex( BOARD_SDRAM_LINUX, 8); TtyPutStr("...\n");
            UsbLedColor( COLOR_LINUX);
            start_linux( "image0", "none");
            return 0; // will branch to Linux
          } else {
            TtyPutStrLn( "*** Image validation failure!");
            GpioTest(); // flash a bunch of lights
            Reset(); // reboot
          }
        } else {
          TtyPutFailure( StrJedec, StrRead);
        }
      } else {
        TtyPutFailure( StrJedec, StrInit);
      }
      //if (AF_Init( AF_BLOCKSIZE)) {
      //  UsbLedColor( COLOR_LOAD);
      //  
      //  if (AF_Read( AF_ADDR_UBOOT, AF_BLOCKS_UBOOT * AF_BLOCKSIZE, (bptr) BOARD_SDRAM_UBOOT) == FLASH_OK) {
      //    if (CheckUbootImage( AF_ADDR_UBOOT, AF_BLOCKS_UBOOT * AF_BLOCKSIZE, (bptr) BOARD_SDRAM_UBOOT, /*e_Silent*/ e_Verbose)) {
      //      TtyPutStr( "Starting u-boot at 0x"); TtyPutHex( BOARD_SDRAM_UBOOT, 8); TtyPutStr("...\n");
      //      UsbLedColor( COLOR_UBOOT);
      //      return 0; // will branch to u-boot
      //    } else {
      //      TtyPutStrLn( "*** Image validation failure!");
      //      GpioTest(); // flash a bunch of lights
      //      Reset(); // reboot
      //    }
      //  } else {
      //    TtyPutFailure( StrAtmel, StrRead);
      //  }
      //} else {
      //  TtyPutFailure( StrAtmel, StrInit);
      //}
    }
    if (MenuSelection && (MenuSelection != '!') && (MenuSelection != ' ')) {
      DisplayMenu = 1;
} } } // no way out of command loop except booting

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

