//-------------------------------------------------------------------------------------------
//  led.c
//-------------------------------------------------------------------------------------------

#include "main.h"

// Use to set the USB LED to a specific color.  Tri-color LED's are active low.

void UsbLedColor( bit32 Color) {
  GpioPut( GPIO_PORTC, LED_WHITE, 1); // extinguish all 3 dies
  GpioPut( GPIO_PORTC, Color    , 0); // illuminate selected dies
}

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

