//-------------------------------------------------------------------------------------------
//  debug.h - rudimentary debug support
//-------------------------------------------------------------------------------------------
//  Don't include this file directly, instead '#define DEBUG' before '#include "main.h"'.
//-------------------------------------------------------------------------------------------

#ifdef DEBUG

  void GpioDebugTrigger( void);

  static bit32 inline DbgGet32( bit32 Address) {
    bit32 Data = * (bit32 *) Address;
    TtyPutHex( Data, 8);
    TtyPutStr(" < ");
    TtyPutHex( Address, 8);
    TtyPutLn();
    return Data;
  }

  static void inline DbgPut32( bit32 Address, bit32 Data) {
    TtyPutStr("           ");
    TtyPutHex( Address, 8);
    TtyPutStr(" < ");
    TtyPutHex( Data, 8);
    TtyPutLn();
    * (bit32 *) Address = Data;
  }

  #define MemGet32 DbgGet32
  #define MemPut32 DbgPut32

  #define DbgPutChr(x)   TtyPutChr(x)
  #define DbgPutStr(x)   TtyPutStr(x)
  #define DbgPutLn()     TtyPutLn()
  #define DbgPutStrLn(x) TtyPutStrLn(x)

  #define DbgPutDec(x,y) TtyPutDec(x,y)
  #define DbgPutHex(x,y) TtyPutHex(x,y)


#else

  #define DbgPutChr(x)
  #define DbgPutStr(x)
  #define DbgPutLn()
  #define DbgPutStrLn(x)

  #define DbgPutDec(x,y)
  #define DbgPutHex(x,y)

#endif // DEBUG

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

