/* bootlin.c
 * example ARM Linux bootloader code
 * this example is distributed under the BSD licence
 */

#include "cache.h"
#include "acnbfx100.h"
#include "main.h"
#include <string.h>

/* list of possible tags */
#define ATAG_NONE       0x00000000
#define ATAG_CORE       0x54410001
#define ATAG_MEM  0x54410002
#define ATAG_RAMDISK    0x54410004
#define ATAG_INITRD2    0x54420005
#define ATAG_SERIAL     0x54410006
#define ATAG_REVISION   0x54410007
#define ATAG_CMDLINE    0x54410009

/* structures for each atag */
struct atag_header {
  u32 size; /* length of tag in words including this header */
  u32 tag;  /* tag type */
};

struct atag_core {
  u32 flags;
  u32 pagesize;
  u32 rootdev;
};

struct atag_mem {
  u32     size;
  u32     start;
};

struct atag_ramdisk {
  u32 flags;
  u32 size;
  u32 start;
};

struct atag_initrd2 {
  u32 start;
  u32 size;
};

struct atag_serialnr {
  u32 low;
  u32 high;
};

struct atag_revision {
  u32 rev;
};

struct atag_cmdline {
  char    cmdline[1];
};

struct atag {
  struct atag_header hdr;
  union {
    struct atag_core   core;
    struct atag_mem    mem;
    struct atag_ramdisk      ramdisk;
    struct atag_initrd2      initrd2;
    struct atag_serialnr     serialnr;
    struct atag_revision     revision;
    struct atag_cmdline      cmdline;
  } u;
};


#define tag_next(t)     ((struct atag *)((u32 *)(t) + (t)->hdr.size))
#define tag_size(type)  ((sizeof(struct atag_header) + sizeof(struct type)) >> 2)
static struct atag *params; /* used to point at the current tag */

static void setup_core_tag( void * address, long pagesize) {
  params = (struct atag *)address;   /* Initialise parameters to start at given address */

  params->hdr.tag = ATAG_CORE;      /* start with the core tag */
  params->hdr.size = tag_size(atag_core); /* size the tag */

  params->u.core.flags = 1;         /* ensure read-only */
  params->u.core.pagesize = pagesize;     /* systems pagesize (4k) */
  params->u.core.rootdev = 0;       /* zero root device (typicaly overidden from commandline )*/

  params = tag_next(params);        /* move pointer to next tag */
}

//static void
//setup_ramdisk_tag(u32_t size)
//{
//    params->hdr.tag = ATAG_RAMDISK;   /* Ramdisk tag */
//    params->hdr.size = tag_size(atag_ramdisk);  /* size tag */
//
//    params->u.ramdisk.flags = 0;      /* Load the ramdisk */
//    params->u.ramdisk.size = size;    /* Decompressed ramdisk size */
//    params->u.ramdisk.start = 0;      /* Unused */
//
//    params = tag_next(params);        /* move pointer to next tag */
//}
//
//static void
//setup_initrd2_tag(u32_t start, u32_t size)
//{
//    params->hdr.tag = ATAG_INITRD2;   /* Initrd2 tag */
//    params->hdr.size = tag_size(atag_initrd2);  /* size tag */
//
//    params->u.initrd2.start = start;  /* physical start */
//    params->u.initrd2.size = size;    /* compressed ramdisk size */
//
//    params = tag_next(params);        /* move pointer to next tag */
//}

static void
setup_mem_tag(u32_t start, u32_t len)
{
    params->hdr.tag = ATAG_MEM;       /* Memory tag */
    params->hdr.size = tag_size(atag_mem);  /* size tag */

    params->u.mem.start = start;      /* Start of memory area (physical address) */
    params->u.mem.size = len;         /* Length of area */

    params = tag_next(params);        /* move pointer to next tag */
}

static void
setup_cmdline_tag(const char * line)
{
    int linelen = (int) StrLen( line);

    if (!linelen)
  return;           /* do not insert a tag for an empty commandline */

    params->hdr.tag = ATAG_CMDLINE;   /* Commandline tag */
    params->hdr.size = (sizeof(struct atag_header) + linelen + 1 + 4) >> 2;

    MemCpy( (bptr) params->u.cmdline.cmdline, (cbptr) line, linelen+1); /* place commandline into tag */

    params = tag_next(params);        /* move pointer to next tag */
}

static void
setup_end_tag(void)
{
    params->hdr.tag = ATAG_NONE;      /* Empty tag ends list */
    params->hdr.size = 0;       /* zero length */
}


#define INITRD_LOAD_ADDRESS BOARD_SDRAM_BASE + 0x800000

static void setup_tags( parameters) {
  setup_core_tag((void *)parameters, 4096);        /* standard core tag 4k pagesize */
  setup_mem_tag(BOARD_SDRAM_BASE, BOARD_SDRAM_SIZE);    /* at 0x20000000 there is 32Mb */
  //setup_ramdisk_tag(4096);         /* create 4Mb ramdisk */ 
  //setup_initrd2_tag(INITRD_LOAD_ADDRESS, 0x100000); /* 1Mb of compressed data placed 8Mb into memory */
  setup_cmdline_tag("mem=32M console=/dev/ttyS0");    /* commandline setting root device */
  setup_end_tag();         /* end of tags */
}

int start_linux( char * name, char * rdname) {
  //void (*theKernel)(int zero, int arch, u32 params);
  //u32 exec_at = (u32)-1;
  u32 parm_at = (u32)-1;
  //u32 machine_type;

  //exec_at = BOARD_SDRAM_LINUX;
  parm_at = BOARD_SDRAM_BASE + 0x100;

  //load_image(name, exec_at);      /* copy image into RAM - already done */

  //load_image(rdname, INITRD_LOAD_ADDRESS);    /* copy initial ramdisk image into RAM  - not used */

  setup_tags(parm_at);        /* sets up parameters */

  //machine_type = MACH_ACFX100;      /* get machine type */

  //irq_shutdown();           /* stop irq - already done */

  //cpu_init_crit();            /* turn MMU off */
//    unsigned int zimage[32];
//    char hd[2];
//    memcpy(zimage, (void *)BOARD_SDRAM_LINUX, 32);
//    TtyPutStr("zImage head: \n");
//    int i;
//    for(i=0; i<32; i++){
//      snprintf(hd, 2, "%1x", zimage[i]);
//      TtyPutStr(hd);
//      if(i%4){
//  TtyPutStr(" ");
//      }
//    }

  TtyPutStr("Booting Linux...\n");
  bit32 *img = (void *) BOARD_SDRAM_LINUX;
  int i;
  for (i=0; i<12; i++){
    TtyPutHex(*img++, 8);
  }

//    bit32 * freq_reg    = (void *) BOARD_CKGR_MCFR;
//    bit32 * plla_reg    = (void *) BOARD_CKGR_PLLAR;
//    bit32 * pmc_mckr    = (void *) BOARD_PMC_MCKR;
//    bit32 * pmc_pck0    = (void *) BOARD_PMC_PCK0;
//    bit32 * pmc_pck1    = (void *) BOARD_PMC_PCK1;
//    bit32 * pmc_sr      = (void *) BOARD_PMC_SR;
//    bit32 * pmc_imr     = (void *) BOARD_PMC_IMR;
//    bit32 * pmc_pllicpr = (void *) BOARD_PMC_PLLICPR;
//    bit32 * pmc_pcr     = (void *) BOARD_PMC_PCR;
//    bit32 mainfrdy = (*freq_reg & 0x10000u)>>16;
//    bit32 freq = (*freq_reg & 0xFFFFu);
//    TtyPutStr("\nFrequency counter ready: ");
//    TtyPutDec(mainfrdy, 1);
//    TtyPutStr("\nFrequency count: ");
//    TtyPutHex(freq, 4);
//    TtyPutStr("\nCKGR_MCFR:       ");
//    TtyPutHex(*freq_reg, 8);
//    TtyPutStr("\nPMC_MCKR:  ");
//    TtyPutHex(*pmc_mckr, 8);
//    TtyPutStr("\nCKGR_PLLAR:      ");
//    TtyPutHex(*plla_reg, 8);
//    TtyPutStr("\nPMC_PCK0:  ");
//    TtyPutHex(*pmc_pck0, 8);
//    TtyPutStr("\nPMC_PCK1:  ");
//    TtyPutHex(*pmc_pck1, 8);
//    TtyPutStr("\nPMC_SR:    ");
//    TtyPutHex(*pmc_sr, 8);
//    TtyPutStr("\nPMC_IMR:   ");
//    TtyPutHex(*pmc_imr, 8);
//    TtyPutStr("\nPMC_PLLICPR:     ");
//    TtyPutHex(*pmc_pllicpr, 8);
//    TtyPutStr("\nPMC_PCR:   ");
//    TtyPutHex(*pmc_pcr, 8);

  TtyPutLn();
  bit32 j;
  bit32 * busm_addr;
  TtyPutStr("Bus Matrix Values:\n");
  for (j=0; j<=0xCCu; j+=4){
    if ((j >= 0x30 && j <= 0x3C) || (j >= 0x68 && j <= 0x7C)) {
      TtyPutStr(".");
      continue;
    }
    TtyPutLn();
    //print Bus Matrix Regjsters
    busm_addr = (void *) BOARD_MATRIX_BASE + j;
    TtyPutHex((bit32) busm_addr, 8);
    TtyPutStr(":  ");
    TtyPutHex(*busm_addr, 8);
  }

//    TtyPutLn();
//    TtyPutStr("MATRIX_MRCR:     ");
//    busm_addr = (void *) BOARD_MATRIX_BASE + 0x100u;
//    TtyPutHex(*busm_addr,8);
//    TtyPutLn();
//    TtyPutStr("CCFG_EBICSA:     ");
//    busm_addr = (void *) BOARD_MATRIX_BASE + 0x120u;
//    TtyPutHex(*busm_addr,8);
//    TtyPutLn();
//    TtyPutStr("MATRIX_WPMR:     ");
//    busm_addr = (void *) BOARD_MATRIX_BASE + 0x1E4u;
//    TtyPutHex(*busm_addr,8);
//    TtyPutLn();
//    TtyPutStr("MATRIX_WPSR:     ");
//    busm_addr = (void *) BOARD_MATRIX_BASE + 0x1E8u;
//    TtyPutHex(*busm_addr,8);
//    TtyPutLn();
//    TtyPutLn();
//
  TtyPutStr(" PMC_SCSR: "); busm_addr = (void *) 0xFFFFFC08; TtyPutHex( *busm_addr, 8); TtyPutLn();
  TtyPutStr(" PMC_PCSR: "); busm_addr = (void *) 0xFFFFFC18; TtyPutHex( *busm_addr, 8); TtyPutLn();
  TtyPutStr("CKGR_UCKR: "); busm_addr = (void *) 0xFFFFFC1C; TtyPutHex( *busm_addr, 8); TtyPutLn();
  TtyPutStr(" CKGR_MOR: "); busm_addr = (void *) 0xFFFFFC20; TtyPutHex( *busm_addr, 8); TtyPutLn();
  TtyPutStr("  PMC_USB: "); busm_addr = (void *) 0xFFFFFC38; TtyPutHex( *busm_addr, 8); TtyPutLn();
  TtyPutStr("  PMC_SMD: "); busm_addr = (void *) 0xFFFFFC3c; TtyPutHex( *busm_addr, 8); TtyPutLn();
  TtyPutStr("  PMC_SMD: "); busm_addr = (void *) 0xFFFFFC3C; TtyPutHex( *busm_addr, 8); TtyPutLn();

  TtyPutStr("\nBooting.\n");
  DelayMs(1000);

//    theKernel = (void (*)(int, int, u32))exec_at; /* set the kernel address */
//    theKernel(0, machine_type, parm_at);    /* jump to kernel with register set */

    return 0;
}
