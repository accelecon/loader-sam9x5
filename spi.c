//-------------------------------------------------------------------------------------------
//  spi.c
//-------------------------------------------------------------------------------------------

#include "main.h"

#define AF_SCBR_MHZ   5  // SPI clock baud rate for AtmelFlash in MHz
#define JF_SCBR_MHZ  10  // SPI clock baud rate for JedecFlash in MHz

#define AF_SCBR   (MasterClock/1000000%AF_SCBR_MHZ == 0 ? MasterClock/1000000/AF_SCBR_MHZ : MasterClock/1000000/AF_SCBR_MHZ+1)
#define JF_SCBR   (MasterClock/1000000%JF_SCBR_MHZ == 0 ? MasterClock/1000000/JF_SCBR_MHZ : MasterClock/1000000/JF_SCBR_MHZ+1)

#define SPI_DLYBS    0 // 16  // delay after chip select in clocks
#define SPI_DLYBCT   0 // 16  // delay between transfers in 32-clocks
#define SPI_DLYBCS   0 //  6  // delay between chip selects in clocks (default)

// Configure hardware for SPI.  This involves setting up the appropriate io pins
// in the PIOA register, the proper clock polarity and timing in the SPI_CSR register,
// and the proper chip select in the SPI_MR register.  Finally, the SPI_CR register
// enables and disables all SPI functions.

#define SPI_MR_BASIS  (AT91C_SPI_MSTR | AT91C_SPI_MODFDIS)

void SPI_SpiInit( bit32 ChipSelect, SpiData * Spi) {
  Spi->ChipSelect = ChipSelect;
  Spi->DataSize = 0;
  MemPut32( BOARD_SPI_CR, AT91C_SPI_SPIDIS); // disable spi

  MemPut32( BOARD_SPI_CR, AT91C_SPI_SWRST); // reset spi
  MemPut32( BOARD_SPI_CR, AT91C_SPI_SWRST); // reset spi again

  MemPut32( BOARD_SPI_MR, AT91C_SPI_MSTR | AT91C_SPI_MODFDIS | AT91C_SPI_PCS); // master mode, no cs selected

  MemPut32( BOARD_SPI_CSR+0, AT91C_SPI_CPOL | (AF_SCBR << 8) | (SPI_DLYBS << 16) | (SPI_DLYBCT << 24)); // AtmelFlash on NPCS0 
  MemPut32( BOARD_SPI_CSR+4, AT91C_SPI_CPOL | (JF_SCBR << 8) | (SPI_DLYBS << 16) | (SPI_DLYBCT << 24)); // JedecFlash on NPCS1

  MemPut32( BOARD_SPI_MR, SPI_MR_BASIS | AT91C_SPI_PCS);
  switch (Spi->ChipSelect) {
    case BOARD_SPI_NPCS0: MemPut32( BOARD_SPI_MR, SPI_MR_BASIS | (0xE << 16)); break; // master mode, cs0 selected
    case BOARD_SPI_NPCS1: MemPut32( BOARD_SPI_MR, SPI_MR_BASIS | (0xD << 16)); break; // master mode, cs1 selected
  }
  MemPut32( BOARD_SPI_CR, AT91C_SPI_SPIEN); // enable spi
}

static void SPI_SpiPut( bit8 Data) {
  while ((MemGet32( BOARD_SPI_SR) & AT91C_SPI_TXEMPTY) == 0) ;
  MemPut32( BOARD_SPI_TDR, Data);
  while ((MemGet32( BOARD_SPI_SR) & AT91C_SPI_TDRE) == 0) ;
}

static bit8 SPI_SpiGet( void) {
  while ((MemGet32( BOARD_SPI_SR) & AT91C_SPI_RDRF) == 0) ;
  return (bit8) (MemGet32( BOARD_SPI_RDR) & 0xff);
}

// Send an SPI SpiCommand with an optional buffer and receive the reply.

SPI_Status SPI_SpiReadWrite( bit32 CmdSize, SpiData * Spi) {
  bit32 Count;
  GpioPut( GPIO_PORTA, Spi->ChipSelect, 0);
  for (Count = 0; Count < CmdSize; Count++) {
    SPI_SpiPut( Spi->Command[Count]);
    Spi->Command[Count] = SPI_SpiGet();
  }
  for (Count = 0; Count < Spi->DataSize; Count++) {
    SPI_SpiPut( Spi->Data[Count]);
    Spi->Data[Count] = SPI_SpiGet();
  }
//MemPut32( BOARD_SPI_CR, AT91C_SPI_LASTXFER);
  GpioPut( GPIO_PORTA, Spi->ChipSelect, 1);
  return SPI_OK;
}

//-------------------------------------------------------------------------------------------
//  end
//-------------------------------------------------------------------------------------------

